# frozen_string_literal: true

source 'https://rubygems.org'

ruby '2.4.3'

git_source(:github) {|repo_name| "https://github.com/#{repo_name}" }

gem 'dotenv'
gem 'foreman', '~> 0.84.0', require: false
gem 'httparty'
gem 'ox'
gem 'oj'
gem 'oj_mimic_json'
gem 'pg'
gem 'puma'
gem 'require_all'
gem 'sidekiq'
gem 'sinatra'
gem 'sneakers', '~> 2.6'
gem 'standalone_migrations'

group :development, :test do
  gem 'derailed_benchmarks'
  gem 'pry-byebug'
  gem 'rubocop', '~> 0.52.1', require: false
  gem 'shotgun'
end

group :test do
  gem 'bunny-mock'
  gem 'database_cleaner'
  gem 'factory_bot'
  gem 'rspec', '~> 3.7'
  gem 'rack-test'
  gem 'vcr', '~> 3.0', '>= 3.0.3'
  gem 'rspec-sidekiq'
  gem 'webmock', '~> 3.1', '>= 3.1.1'
end
