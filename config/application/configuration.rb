require 'singleton'

class AppConfiguration
  include Singleton
  attrs = %i[log_level logger_output rabbitmq_uri redis_url]
  attr_accessor *attrs
end
