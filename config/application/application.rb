class Application
  def self.configure
    yield(self.config)
  end

  def self.config
    AppConfiguration.instance
  end

  def self.database_config
    yaml = ROOT_PATH.join('config/database.yml')

    config = if yaml.exist?
      require "yaml"
      require "erb"
      YAML.load(ERB.new(yaml.read).result) || {}
    else
      raise "Could not load database configuration. No such file - config/database.yml"
    end

    config
  rescue Psych::SyntaxError => e
    raise "YAML syntax error occurred while parsing config/database.yml. " \
          "Please note that YAML must be consistently indented using spaces. Tabs are not allowed. " \
          "Error: #{e.message}"
  rescue => e
    raise e, "Cannot load `Application.database_config`:\n#{e.message}", e.backtrace
  end

  def self.init
    require_all ROOT_PATH.join("config/initializers", '**/*.rb')
  end
end
