Application.configure do |config|
  config.logger_output = STDOUT
  config.rabbitmq_uri = ENV.fetch('RABBITMQ_URL')
  config.redis_url = ENV.fetch('REDIS_URL')
end
