Application.configure do |config|
  config.logger_output = 'log/tagged_dev.log'
  config.rabbitmq_uri = 'amqp://guest:guest@localhost:5672'
  config.redis_url = 'redis://localhost:6379'
end
