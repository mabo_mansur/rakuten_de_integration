begin
  require_all ROOT_PATH.join('app/models', '**/*.rb')
rescue LoadError
  nil
end
ActiveRecord::Base.establish_connection(Application.database_config.fetch(APP_ENV))
