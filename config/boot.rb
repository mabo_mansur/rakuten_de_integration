require 'dotenv/load'
require 'rubygems'
require 'bundler/setup'
require 'logger'
require 'sinatra'
require 'sneakers'

ROOT_PATH = Pathname.new(::File.expand_path('../../', __FILE__)).freeze
APP_ENV = ENV.fetch('RACK_ENV', 'development').freeze

Bundler.require :default, APP_ENV

require ROOT_PATH.join('config/application/configuration')
require ROOT_PATH.join('config/application/application')
require ROOT_PATH.join("config/environments/#{APP_ENV}")

require_all ROOT_PATH.join('lib', '**/*.rb')
require_all Dir.glob("app/**/*.rb").reject { |f| f.include?('models') }

Application.init
