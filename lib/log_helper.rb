module LogHelper
  class RakutenLogger
    def initialize(tags)
      @tags = tags
    end

    def logger
      @logger ||= Logger.new(Application.config.logger_output)
    end

    def log(type, text)
      log_by_type(logger, type, @tags, text)
    end

    private

    def log_by_type(logger, type, tags, text)
      types = {
        warn: WarnLogger,
        info: InfoLogger,
        debug: DebugLogger,
        fatal: FatalLogger,
        error: ErrorLogger
      }

      log_text = prepared_text(tags, text)
      logger_class = types.fetch(type)
      logger_class.log(logger, log_text)
    end

    def prepared_text(tags, text)
      ''.tap do |s|
        tags.each do |t|
          s << ("[#{t}]" + ' ')
          s << text if tags.last == t
        end
      end
    end

    class ErrorLogger
      def self.log(logger, text)
        logger.error(text)
      end
    end

    class InfoLogger
      def self.log(logger, text)
        logger.info(text)
      end
    end

    class WarnLogger
      def self.log(logger, text)
        logger.warn(text)
      end
    end

    class DebugLogger
      def self.log(logger, text)
        logger.debug(text)
      end
    end

    class FatalLogger
      def self.log(logger, text)
        logger.fatal(text)
      end
    end
  end

  def tag_logger(*tags)
    @tag_logger ||= RakutenLogger.new(tags)
  end

  def log(type, text)
    @tag_logger.log(type, text)
  end
end
