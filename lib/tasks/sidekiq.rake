namespace :sidekiq do
  namespace :sync do
    desc 'Start products synchronization'
    task products: :environment do
      User.where(veeqo_channel_active: true).ids.each do |id|
        Products::InitSynchronizerWorker.perform_async(id)
      end
    end

    desc 'Start orders synchronization'
    task orders: :environment do
      User.where(veeqo_channel_active: true).ids.each do |id|
        Orders::InitSynchronizerWorker.perform_async(id)
      end
    end
  end
end
