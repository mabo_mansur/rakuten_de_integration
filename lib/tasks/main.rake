desc 'Starts irb session with loaded application'
task console: :environment do
  require './config/boot.rb'
  require 'irb'
  ARGV.clear
  IRB.start
end

task c: :console
