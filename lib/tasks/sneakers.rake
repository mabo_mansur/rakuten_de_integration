require './config/boot'
require 'sneakers'
require 'sneakers/runner'

ERROR_MESSAGE =
  "
    Error: No workers found. \n \
    Please set the classes of the workers you want to run like so: \n\n \
    $ export WORKERS=MyWorker,FooWorker \n \
    $ rake sneakers:run \n \
  ".freeze

task :environment

namespace :sneakers_custom do
  desc 'Start work (set $WORKERS=Klass1,Klass2)'
  task :run do
    Sneakers.server = true
    Rake::Task['environment'].invoke

    if ENV['WORKERS'].nil?
      workers = Sneakers::Worker::Classes
    else
      workers, missing_workers = Sneakers::Utils.parse_workers(ENV['WORKERS'])
    end

    unless missing_workers.nil? || missing_workers.empty?
      puts "Missing workers: #{missing_workers.join(', ')}" if missing_workers
      puts 'Did you `require` properly?'
      exit(1)
    end

    if workers.empty?
      puts ERROR_MESSAGE
      exit(1)
    end
    opts = (!ENV['WORKER_COUNT'].nil? ? {workers: ENV['WORKER_COUNT'].to_i} : {})
    r = Sneakers::Runner.new(workers, opts)

    r.run
  end
end
