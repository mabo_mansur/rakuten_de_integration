class VeeqoClient
  include LogHelper

  API_URL = 'https://api.veeqo.com/'.freeze

  def initialize(user_token)
    @user_token = user_token
  end

  def validate_user
    get_request('current_company', @user_token)
  end

  private

  def get_request(endpoint, user_token)
    tag_logger('VeeqoClient#GET_request')
    response = HTTParty.get(
      API_URL + endpoint,
      headers: {'X-Api-Key' => user_token, 'X-Api-Request' => 'true'}
    )
    log(:info, "method: GET | url: #{response.request.uri}")
    log(:info, "response code #{response.code}")

    response
  end
end
