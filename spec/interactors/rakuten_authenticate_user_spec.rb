RSpec.describe RakutenAuthenticateUser, vcr: true do
  it 'get_key_info success' do
    response = described_class.call('123456789a123456789a123456789a12')

    expect(response[:status]).to eq('success')
  end
  it 'get_key_info failure' do
    response = described_class.call('some_key')
    expect(response[:status]).to eq('fail')
  end
end
