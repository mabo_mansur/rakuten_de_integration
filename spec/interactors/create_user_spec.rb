RSpec.describe CreateUser do
  it 'create success' do
    params = {
      rakuten_auth_key: SANDBOX_RAKUTEN_API_KEY,
      veeqo_user_token: 'veeqo_user_token',
      veeqo_channel_token: 'veeqo_channel_token'
    }

    first = described_class.call(params)
    expect(first[:status]).to eq('success')
    expect(User.where(rakuten_auth_key: SANDBOX_RAKUTEN_API_KEY).count).to eq(1)

    # second user not be created with same credentials
    second = described_class.call(params)
    expect(second[:status]).to eq('fail')
    expect(second[:msg]).to eq('User not created')
  end
end
