FactoryBot.define do
  to_create { |instance| instance&.save }

  factory :user do
    veeqo_user_token      'user_token'
    veeqo_channel_token   'channel_token'
    rakuten_auth_key      SANDBOX_RAKUTEN_API_KEY
    veeqo_channel_active  true
  end
end
