describe Product, '.identical?', type: :model do
  let!(:product_hash) { RakutenFixtureData::Products.product }
  let!(:product) { Product.create(data: product_hash) }

  context 'compare hash with data' do
    it { expect(product.identical?(product_hash)).to be(true) }
  end

  context 'compare symbolized hash with data' do
    it { expect(product.identical?(product_hash.deep_symbolize_keys)).to be(true) }
  end

  context 'compare hash after sidekiq transformations' do
    let(:compare_hash) { JSON.load(JSON.dump(product_hash)) }
    it { expect(product.identical?(compare_hash)).to be(true) }
  end

  context 'compare non identic hash in deep layer' do
    context 'when change attribute' do
      let(:compare_hash) do
        h = product_hash
        h[:variants][:variant][:price_reduced] = '0.89'
        h
      end

      it { expect(product.identical?(compare_hash)).to be(false) }
    end

    context 'when add new line in array' do
      let(:compare_hash) do
        h = product_hash
        h[:variants][:variant] = [
          {variant_id: '5', variant_art_no: 'VART-88', name: 'Blau', price: '1.99', price_reduced: '0.99', price_reduced_type: 'VK', baseprice_unit: 'ml', baseprice_volume: '0.750', stock: '20', delivery: '3', available: '1', isbn: '123456789', ean: '123456789', mpn: '123456789', sort: '2'},
          {variant_id: '6', variant_art_no: 'VART-89', name: 'Blau', price: '1.99', price_reduced: '0.99', price_reduced_type: 'VK', baseprice_unit: 'ml', baseprice_volume: '0.750', stock: '20', delivery: '3', available: '1', isbn: '123456789', ean: '123456789', mpn: '123456789', sort: '2'}
        ]
        h
      end

      it { expect(product.identical?(compare_hash)).to be(false) }
    end
  end

  context 'compare identic hash with different sort' do
    let(:compare_hash) do
      h = product_hash
      h = h.sort.to_h
      h[:variants][:variant] = {variant_art_no: 'VART-88', variant_id: '5', name: 'Blau', price: '1.99', price_reduced: '0.99', price_reduced_type: 'VK', baseprice_unit: 'ml', baseprice_volume: '0.750', stock: '20', delivery: '3', available: '1', isbn: '123456789', ean: '123456789', mpn: '123456789', sort: '2'}
      h
    end

    it { expect(product.identical?(compare_hash)).to be(true) }
  end
end
