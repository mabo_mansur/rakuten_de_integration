module RakutenFixtureData
  FIXTURES_PATH = 'spec/fixtures/data/'.freeze

  def self.parse_yml(entity_type, filename)
    file = File.read(FIXTURES_PATH + entity_type << '/' << filename << '.yml')
    Oj.load(file).deep_symbolize_keys
  end

  def self.parse(filename)
    eval(File.read(FIXTURES_PATH + filename))
  end

  module Orders
    class << self
      def empty
        RakutenFixtureData.parse('orders/empty.rb')
      end

      def processing_one
        RakutenFixtureData.parse('orders/processing_one.rb')
      end

      def processing_hash
        JSON.load(JSON.dump(processing_one[:orders][:order]))
      end

      def real_hash
        JSON.load(JSON.dump(RakutenFixtureData.parse('orders/real_one.rb')[:orders][:order]))
      end

      def extended_processing_hash
        JSON.load(JSON.dump(RakutenFixtureData.parse('orders/extended_processing_one.rb')[:orders][:order]))
      end

      def processing(page)
        raise RakutenFixtureData::OrdersError, 'invalid page' unless (1..3).cover?(page)
        RakutenFixtureData.parse("orders/processing_page#{page}.rb")
      end

      def unauthorised
        RakutenFixtureData.parse('unauthorised.rb')
      end

      def unexpected_error
        RakutenFixtureData.parse('unexpected_error.rb')
      end

      def many_codes_error
        RakutenFixtureData.parse('many_codes_error.rb')
      end
    end
  end

  module Products
    class << self
      def one
        RakutenFixtureData.parse('products/one.rb')
      end

      def one_page
        RakutenFixtureData.parse('products/one_page.rb')
      end

      def product_empty
        RakutenFixtureData.parse('products/empty.rb')
      end

      def product
        RakutenFixtureData.parse('products/product.rb')
      end

      def all(page)
        raise RakutenFixtureData::ProductsError, 'invalid page' unless (1..3).cover?(page)
        RakutenFixtureData.parse("products/per_page100_page#{page}.rb")
      end
    end
  end

  OrdersError = Class.new(StandardError)
  ProductsError = Class.new(StandardError)
end
