RSpec.shared_context 'publisher_mock' do
  before(:each) do
    BunnyMock.use_bunny_queue_pop_api = true
    @bunny_mock = BunnyMock.new
    allow(Publisher).to receive(:bunny) { @bunny_mock }
  end
end
