RSpec.describe Orders::UpdateWorker do
  let(:user) { create :user }
  let!(:serv_double) { double }

  context 'cancel order' do
    let(:channel_message) do
      Oj.dump(
        user_token: user.veeqo_user_token,
        channel_token: user.veeqo_channel_token,
        payload: {
          remote_order_id: '123-123-123',
          action: 'cancel',
          cancelled_at: '2017-11-23 16:30 UTC',
          cancel_reason: 'Product is out of stock, sorry',
          notify_client: false
        }
      )
    end

    before do
      allow(Orders::CancelSynchronizer).to receive(:new).with(Oj.load(channel_message, symbol_keys: true)).and_return(serv_double)
      allow(serv_double).to receive(:to_rakuten)

      described_class.perform_async(channel_message)
    end

    it { expect(serv_double).to have_received(:to_rakuten).once }
  end

  context 'ship order' do
    let(:channel_message) do
      Oj.dump(
        user_token: 'user_token',
        channel_token: 'channel_token',
        payload: {
          remote_order_id: '123-123-123',
          action: 'ship',
          shipments: [
            {
              carrier: 'FedEx',
              carrier_service: '2nd day delivery',
              tracking_number: '111222',
              created_at: '2017-06-20 14:30 UTC'
            },
            {
              carrier: 'FedEx',
              carrier_service: '2nd day delivery',
              tracking_number: '33334444',
              created_at: '2017-06-20 14:31 UTC'
            }
          ]
        }
      )
    end

    before do
      allow(Orders::ShipSynchronizer).to receive(:new).with(Oj.load(channel_message, symbol_keys: true)).and_return(serv_double)
      allow(serv_double).to receive(:to_rakuten)

      described_class.perform_async(channel_message)
    end

    it { expect(serv_double).to have_received(:to_rakuten).once }
  end
end
