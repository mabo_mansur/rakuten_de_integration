RSpec.describe Orders::ImportCallbacksWorker do
  let!(:user) { create :user }
  let!(:order) { create :order, user_id: user.id, rakuten_id: '100-000-000' }
  let!(:serv_double) { double }

  context 'when valid attributes' do
    before do
      allow(Orders::LineItemsSynchronizer).to receive(:new).with(user, order).and_return(serv_double)
      allow(serv_double).to receive(:synchronize)

      described_class.perform_async(channel_message)
    end

    context 'repeat import order products' do
      let(:channel_message) do
        Oj.dump(
          user_token: user.veeqo_user_token,
          channel_token: user.veeqo_channel_token,
          payload: {
            remote_order_id: order.rakuten_id,
            message: 'contains_not_pulled_products'
          }
        )
      end

      it { expect(serv_double).to have_received(:synchronize).once }
    end

    context 'any other status' do
      let(:channel_message) do
        Oj.dump(
          user_token: user.veeqo_user_token,
          channel_token: user.veeqo_channel_token,
          payload: {
            remote_order_id: order.rakuten_id,
            message: 'blablabla_status'
          }
        )
      end

      it { expect(serv_double).to have_received(:synchronize).exactly(0) }
    end
  end

  context 'when invalid attributes' do
    context 'when user not exist' do
      let!(:no_user_message) do
        Oj.dump(
          user_token: '',
          channel_token: '',
          payload: {
            remote_order_id: order.rakuten_id,
            message: 'contains_not_pulled_products'
          }
        )
      end

      it 'should raise error' do
        expect { described_class.perform_async(no_user_message) }.to raise_error(ActiveRecord::RecordNotFound, 'Couldn\'t find User')
      end
    end

    context 'when order not exist' do
      let!(:no_order_message) do
        Oj.dump(
          user_token: user.veeqo_user_token,
          channel_token: user.veeqo_channel_token,
          payload: {
            remote_order_id: '',
            message: 'contains_not_pulled_products'
          }
        )
      end

      it 'should raise error' do
        expect { described_class.perform_async(no_order_message) }.to raise_error(ActiveRecord::RecordNotFound, /Couldn't find Order/)
      end
    end
  end
end
