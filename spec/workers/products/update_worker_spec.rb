RSpec.describe Products::UpdateWorker do
  let(:user) { create :user }
  let(:product) { create :product, user_id: user.id }
  let!(:serv_double) { double }

  context 'update stock level' do
    let(:channel_message) do
      Oj.dump(
        user_token: user.veeqo_user_token,
        channel_token: user.veeqo_channel_token,
        payload: {
          remote_product_id: '111111',
          remote_variant_id: '222222',
          remote_sku: 'SKU-2',
          action: 'update_stock_level',
          quantity: 99,
          infinite: false,
          inventory_tracking: true
        }
      )
    end

    before do
      allow(Products::InventorySynchronizer).to receive(:new).and_return(serv_double)
      allow(serv_double).to receive(:synchronize)

      described_class.perform_async(channel_message)
    end

    it 'successfully' do
      expect(serv_double).to have_received(:synchronize).once
    end
  end

  context 'update price' do
    let(:channel_message) do
      Oj.dump(
        user_token: user.veeqo_user_token,
        channel_token: user.veeqo_channel_token,
        payload: {
          remote_product_id: '111111',
          remote_variant_id: '222222',
          remote_sku: 'SKU-2',
          action: 'price',
          quantity: 99,
          infinite: false,
          inventory_tracking: true
        }
      )
    end

    before do
      allow(Products::InventorySynchronizer).to receive(:new).and_return(serv_double)
      allow(serv_double).to receive(:synchronize)

      described_class.perform_async(channel_message)
    end

    it 'not run service' do
      expect(serv_double).to have_received(:synchronize).exactly(0)
    end
  end
end
