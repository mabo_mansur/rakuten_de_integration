require 'vcr'
require 'pry'
require 'webmock/rspec'
require 'rspec'
require 'rack/test'
require 'bunny-mock'
require 'factory_bot'
require 'sidekiq/testing'
require 'database_cleaner'

ENV['RACK_ENV'] = 'test'
SANDBOX_RAKUTEN_API_KEY = '123456789a123456789a123456789a12'.freeze

require_relative '../config/boot'

Dir[ROOT_PATH.join('spec/support/**/*.rb')].each { |f| require f }

Sidekiq::Testing.inline!

RSpec::Sidekiq.configure do |config|
  config.warn_when_jobs_not_processed_by_sidekiq = false
end

VCR.configure do |config|
  config.cassette_library_dir = 'spec/fixtures/vcr_cassettes'
  config.hook_into :webmock
  config.configure_rspec_metadata!
  config.default_cassette_options = {record: :new_episodes}
  config.allow_http_connections_when_no_cassette = true
end

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end
  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.include FactoryBot::Syntax::Methods
  config.include Rack::Test::Methods
  config.include RSpecMixin
  config.shared_context_metadata_behavior = :apply_to_host_groups
  config.before(:suite) do
    FactoryBot.find_definitions
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
  end

  config.around(:each) do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end
end
