RSpec.describe ProductSerializer, type: :service, vcr: true do
  include_context 'publisher_mock'
  let(:user) { create(:user) }
  let(:products_client) { Rakuten::Client.new(user).products(1) }

  describe 'prepare' do
    it 'returns product hash in veeqo format' do
      products = products_client[:products][:product]
      json = ProductSerializer.new(products[0]).prepare

      expect(json[:id]).to eq(products[0][:product_id])
      expect(json[:sku_code]).to eq(products[0][:product_art_no])
      expect(json[:currency]).to eq('EUR')
      expect(json[:title]).to eq(products[0][:name])
      expect(json[:description]).to eq(products[0][:description])
      expect(json[:brand]).to eq(products[0][:producer])
      expect(json[:price_inc_tax]).to eq(products[0][:price])
      expect(json[:price_ex_tax]).to eq('2.51260504201680672268907563')
      expect(json[:tax_rate]).to eq(0.19)

      expect(json[:special_price_inc_tax]).to eq(products[0][:price_reduced])
      expect(json[:special_price_ex_tax]).to eq('1.672268907563025210084033614')

      expect(json[:inventory_tracking]).to eq(true)
      expect(json[:active]).to eq(true)
      expect(json[:dimensions_unit]).to eq(products[0][:baseprice_unit])
      expect(json[:available_quantity]).to eq(products[0][:stock])
      expect(json[:created_at]).to eq(products[0][:created])
      expect(json[:images][0][:id]).to eq(products[0][:images][:image][:image_id])
      expect(json[:images][0][:url]).to eq(products[0][:images][:image][:src])
    end

    it 'returns product hash in veeqo format with variants' do
      products = products_client[:products][:product]
      variant = products[1][:variants][:variant]
      json = ProductSerializer.new(products[1]).prepare
      variants_json = json[:variants].first

      expect(variants_json[:id]).to eq(variant[:product_id])
      expect(variants_json[:sku_code]).to eq(variant[:product_art_no])
      expect(variants_json[:title]).to eq(variant[:name])
      expect(variants_json[:description]).to eq(variant[:description])
      expect(variants_json[:brand]).to eq(variant[:producer])
      expect(variants_json[:price_inc_tax]).to eq(variant[:price])
      expect(variants_json[:price_ex_tax]).to eq('1.672268907563025210084033614')
      expect(variants_json[:special_price_inc_tax]).to eq('0.99')
      expect(variants_json[:special_price_ex_tax]).to eq('0.831932773109243697478991596638655463')

      expect(json[:inventory_tracking]).to eq(false)
      expect(json[:active]).to eq(true)
      expect(variants_json[:dimensions_unit]).to eq(variant[:baseprice_unit])
      expect(variants_json[:available_quantity]).to eq(variant[:stock])
      expect(variants_json[:created_at]).to eq(variant[:created])

      expect(variants_json[:created_at]).to eq(variant[:created])
    end
  end

  describe 'product with variants has correct options and variant specifications' do
    it 'when full specs filled out' do
      product = RakutenFixtureData.parse_yml('products', 'product_with_variants_1')
      json = ProductSerializer.new(product).prepare

      expect(json[:options]).to eq(
        [
          {id: '1', name: '32', required: true},
          {id: '2', name: '34', required: true},
          {id: '3', name: '65', required: true},
          {id: '4', name: '45', required: true},
          {id: '5', name: '23', required: true}
        ]
      )
      expect(json[:variants][0][:specifics]).to eq(
        [
          {id: '1', option_id: '1', value: 'value1'},
          {id: '2', option_id: '2', value: 'value2'},
          {id: '3', option_id: '3', value: 'value3'},
          {id: '4', option_id: '4', value: 'value4'},
          {id: '5', option_id: '5', value: 'value5'}
        ]
      )
    end

    it 'when less then 5 specs filled' do
      product = RakutenFixtureData.parse_yml('products', 'product_with_variants_2')
      json = ProductSerializer.new(product).prepare

      expect(json[:options]).to eq([{id: '1', name: 'Größe', required: true}])
      expect(json[:variants][0][:specifics]).to eq([{id: '1', option_id: '1', value: '15 x 20'}])
    end
  end
end
