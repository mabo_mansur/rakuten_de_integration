RSpec.describe OrderSerializer, '#serialize', type: :service, vcr: true do
  include_context 'publisher_mock'

  let!(:user) { create(:user) }

  subject { OrderSerializer.new(order_json).serialize }

  context 'hash with one item' do
    let(:order_json) { RakutenFixtureData::Orders.processing_hash.deep_symbolize_keys }

    it 'returns order hash in veeqo format' do
      expect(subject[:id]).to eq('100-000-000')
      expect(subject[:number]).to eq('123456')
      expect(subject[:due_date]).to eq('2010-01-01 20:15:00')
      expect(subject[:currency]).to eq('EUR')
      expect(subject[:warehouse_id]).to be_nil
      expect(subject[:outlet_id]).to be_nil
      expect(subject[:status]).to eq('refunded')
      expect(subject[:created_at]).to eq('2010-01-01 20:15:00')
      expect(subject[:updated_at]).to be_nil
      expect(subject[:cancelled_at]).to be_nil
      expect(subject[:cancel_reason]).to be_nil
      expect(subject[:shipped_at]).to be_nil
      expect(subject[:employee_notes]).to eq([{text: 'Beim Lieferanten bestellt'}])
      expect(subject[:customer_note]).to eq('Ich freu mich so sehr!')
      expect(subject[:delivery_method]).to be_nil
      expect(subject[:billing_address]).to be_nil

      expect(subject[:customer][:id]).to eq('1')
      expect(subject[:customer][:email]).to eq('max@mustermann.de')
      expect(subject[:customer][:first_name]).to eq('Max')
      expect(subject[:customer][:second_name]).to eq('Mustermann')
      expect(subject[:customer][:phone]).to eq('123456-4555')

      expect(subject[:shipping_address][:id]).to be_nil
      expect(subject[:shipping_address][:first_name]).to eq('D Max')
      expect(subject[:shipping_address][:last_name]).to eq('D Mustermann')
      expect(subject[:shipping_address][:company]).to eq('D Muster GmbH')
      expect(subject[:shipping_address][:address1]).to eq('D Musterstra\u00dfe D 1')
      expect(subject[:shipping_address][:address2]).to eq('D Seiteneingang')
      expect(subject[:shipping_address][:city]).to eq('D Musterstadt')
      expect(subject[:shipping_address][:country]).to eq('D DE')
      expect(subject[:shipping_address][:state]).to be_nil
      expect(subject[:shipping_address][:zip]).to eq('D 11111')
      expect(subject[:shipping_address][:phone]).to be_nil
      expect(subject[:shipping_address][:email]).to be_nil
      expect(subject[:shipping_address][:created_at]).to be_nil
      expect(subject[:shipping_address][:updated_at]).to be_nil

      expect(subject[:payments]).to eq([{id: nil, payment_method: 'Kreditkarte', reference_number: nil}])

      expect(subject[:line_items].count).to eq(1)
      expect(subject[:line_items].first[:product_id]).to eq('4')
      expect(subject[:line_items].first[:id]).to eq('1')
      expect(subject[:line_items].first[:price_per_unit_inc_tax]).to be_nil
      expect(subject[:line_items].first[:price_per_unit_ex_tax]).to eq('8.403361344537815126050420169')
      expect(subject[:line_items].first[:tax_rate]).to eq('0.19')
      expect(subject[:line_items].first[:quantity]).to eq(order_json[:items][:item][:qty])
      expect(subject[:line_items].first[:discount_amount]).to eq('0.0')
      expect(subject[:line_items].first[:additional_options]).to eq([])

      expect(subject[:totals][:delivery_cost_inc_tax]).to be_nil
      expect(subject[:totals][:delivery_cost_ex_tax]).to eq('2.536231884057971014492753624')
      expect(subject[:totals][:delivery_cost_tax_rate]).to eq('0.38')
      expect(subject[:totals][:delivery_cost_discount]).to eq('0.0')
      expect(subject[:totals][:order_discount]).to eq('8.403361344537815126050420169')
      expect(subject[:totals][:total_discounts]).to eq('8.403361344537815126050420169')
      expect(subject[:totals][:total_price_inc_tax]).to eq('12.12') # order[:total]
      expect(subject[:totals][:total_price_ex_tax]).to eq('9.55959322859578614054317379078')
      expect(subject[:totals][:total_fees]).to eq('0.0')
      expect(subject[:totals][:tax_rate]).to eq('0.267836372341160106617310764146163781113833082701101536')
      expect(subject[:totals][:total_tax]).to eq('2.56040677140421385945682620922')

      expect(subject[:shipments]).to eq([])
    end
  end

  context 'hash with many items' do
    let(:order_json) { RakutenFixtureData::Orders.extended_processing_hash.deep_symbolize_keys }

    it 'returns order hash in veeqo format' do
      expect(subject[:id]).to eq('100-000-000')
      expect(subject[:number]).to eq('100-000-000')
      expect(subject[:due_date]).to eq('2010-01-01 20:15:00')
      expect(subject[:currency]).to eq('EUR')
      expect(subject[:warehouse_id]).to be_nil
      expect(subject[:outlet_id]).to be_nil
      expect(subject[:status]).to eq('refunded')
      expect(subject[:created_at]).to eq('2010-01-01 20:15:00')
      expect(subject[:updated_at]).to be_nil
      expect(subject[:cancelled_at]).to be_nil
      expect(subject[:cancel_reason]).to be_nil
      expect(subject[:shipped_at]).to be_nil
      expect(subject[:employee_notes]).to eq([{text: 'Beim Lieferanten bestellt'}])
      expect(subject[:customer_note]).to eq('Ich freu mich so sehr!')
      expect(subject[:delivery_method]).to be_nil
      expect(subject[:billing_address]).to be_nil

      expect(subject[:customer][:id]).to eq('1')
      expect(subject[:customer][:email]).to eq('max@mustermann.de')
      expect(subject[:customer][:first_name]).to eq('Max')
      expect(subject[:customer][:second_name]).to eq('Mustermann')
      expect(subject[:customer][:phone]).to eq('123456-4555')

      expect(subject[:shipping_address][:id]).to be_nil
      expect(subject[:shipping_address][:first_name]).to eq('D Max')
      expect(subject[:shipping_address][:last_name]).to eq('D Mustermann')
      expect(subject[:shipping_address][:company]).to eq('D Muster GmbH')
      expect(subject[:shipping_address][:address1]).to eq('D Musterstra\u00dfe D 1')
      expect(subject[:shipping_address][:address2]).to eq('D Seiteneingang')
      expect(subject[:shipping_address][:city]).to eq('D Musterstadt')
      expect(subject[:shipping_address][:country]).to eq('D DE')
      expect(subject[:shipping_address][:state]).to be_nil
      expect(subject[:shipping_address][:zip]).to eq('D 11111')
      expect(subject[:shipping_address][:phone]).to be_nil
      expect(subject[:shipping_address][:email]).to be_nil
      expect(subject[:shipping_address][:created_at]).to be_nil
      expect(subject[:shipping_address][:updated_at]).to be_nil

      expect(subject[:payments]).to eq([{id: nil, payment_method: 'Paypal', reference_number: nil}])

      expect(subject[:line_items].count).to eq(6)
      expect(subject[:line_items].first[:product_id]).to eq('1268012851')
      expect(subject[:line_items].first[:variant_id]).to eq('1268012851') # because :variant_id=>"0"
      expect(subject[:line_items].second[:product_id]).to eq('1256939976')
      expect(subject[:line_items].second[:variant_id]).to eq('1489964686')

      expect(subject[:totals][:delivery_cost_inc_tax]).to be_nil
      expect(subject[:totals][:delivery_cost_ex_tax]).to eq('2.786777299333302608722175003159152552598569571642192955980802815710863132204014102578680292263614227951025928')
      expect(subject[:totals][:delivery_cost_tax_rate]).to eq('0.255930999881951787552685889476596349758067977009911178302658570319301270197828427786415938')
      expect(subject[:totals][:delivery_cost_discount]).to eq('0.0')

      check_shipping =  subject[:totals][:delivery_cost_ex_tax].to_f *
                        subject[:totals][:delivery_cost_tax_rate].to_f +
                        subject[:totals][:delivery_cost_ex_tax].to_f
      expect(order_json[:shipping].to_d).to eq(check_shipping.to_d)

      expect(subject[:totals][:order_discount]).to eq('14.915033405514117630194541279986244753719641565362758187387104374460064401')
      expect(subject[:totals][:total_discounts]).to eq('14.915033405514117630194541279986244753719641565362758187387104374460064401')

      expect(subject[:totals][:total_price_inc_tax]).to eq('34.58') # order[:total]
      expect(subject[:totals][:total_price_ex_tax]).to eq('29.456209455744889839911439581622907798878928006279434768593698441250798731204014102578680292263614227951025928')

      expect(subject[:totals][:total_fees]).to eq('0.0')
      expect(subject[:totals][:tax_rate]).to eq('0.173946024927379425895217361359074186929225713529752252763214193421267618469948897782584614984663374166071203869382444210490354')
      expect(subject[:totals][:total_tax]).to eq('5.123790544255110160088560418377092201121071993720565231406301558749201268795985897421319707736385772048974072')

      expect(subject[:shipments]).to eq([])
    end
  end

  context 'hash with production data' do
    let(:order_json) { RakutenFixtureData::Orders.real_hash.deep_symbolize_keys }

    it 'returns order hash in veeqo format' do
      expect(subject[:id]).to eq('628-799-492')

      expect(subject[:totals][:delivery_cost_ex_tax]).to eq('0.0')
      expect(subject[:totals][:delivery_cost_tax_rate]).to eq('0.125210555563085347176102248547481127143560656627089983')
      expect(subject[:totals][:delivery_cost_discount]).to eq('0.0')

      expect(subject[:totals][:order_discount]).to eq('0.0')
      expect(subject[:totals][:total_discounts]).to eq('0.0')

      # total_price_inc_tax = total_price_ex_tax + total_tax
      expect(subject[:totals][:total_price_inc_tax].to_f).to eq(order_json[:total].to_f)

      # all_subtotals_sum - total_discounts + delivery_cost_ex_tax
      # {qty: '1', price: '13.39', tax: '2'}, {qty: '1', price: '12.69', tax: '1'}
      # tax: '2' = 7%, tax: '1' = 19%
      # 13.39 / 1.07 + 12.69 / 1.19 = 23.17788423780727
      expect(subject[:totals][:total_price_ex_tax]).to eq('23.17788423780727244168695515573')

      # 13.39 / 1.07 * 0.07 + 12.69 / 1.19 * 0.19 = 2.902115762192728
      expect(subject[:totals][:total_tax]).to eq('2.90211576219272755831304484427')

      # tax_rate = total_tax / total_price_ex_tax
      # 2.902115762192728 / 23.17788423780727 = 0.12521055556308539
      expect(subject[:totals][:tax_rate]).to eq('0.125210555563085347176102248554341865765498054031243843')

      # 23.177884237807272441686955157 + 2.90211576219272755831304484427 = 26.08
      expect(order_json[:total]).to eq('26.08')
    end
  end
end
