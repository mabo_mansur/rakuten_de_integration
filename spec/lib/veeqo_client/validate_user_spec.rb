RSpec.describe 'VeeqoClient#validateUser', vcr: true do
  context 'get request info from remote server with valid params' do
    it 'should return success response' do
      data = VeeqoClient.new('valid_token').validate_user

      expect(data.success?).to eq(true)
    end
  end

  context 'get request info from remote server with invalid params' do
    it 'should return fail response' do
      data = VeeqoClient.new('invalid_token').validate_user

      expect(data.success?).to eq(false)
      expect(data.parsed_response['error_messages']).to eq('You\'re not authorized')
    end
  end
end
