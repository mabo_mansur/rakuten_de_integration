RSpec.describe Producers::Auth, type: :service, vcr: true, sidekiq: :inline do
  include_context 'publisher_mock'

  let(:user) { create(:user) }
  let(:producer) { Producers::Auth.new(user) }
  let(:fail_reason) { 'Some fail reason' }
  let(:queue) { @bunny_mock.channel.queue('rakuten.authorisation.callbacks') }

  it '.auth_success' do
    expect { producer.auth_success }.to change { queue.message_count }.by(1)
    pushed_message = JSON.parse(queue.pop[2]).deep_symbolize_keys!

    expect(pushed_message[:user_token]).to eq(user.veeqo_user_token)
    expect(pushed_message[:payload][:result_code]).to eq('success')
  end

  it '.auth_failed' do
    expect { producer.auth_failed(fail_reason) }.to change { queue.message_count }.by(1)
    pushed_message = JSON.parse(queue.pop[2]).deep_symbolize_keys!

    expect(pushed_message[:user_token]).to eq(user.veeqo_user_token)
    expect(pushed_message[:payload][:result_code]).to eq('failure')
    expect(pushed_message[:payload][:result_message]).to eq(fail_reason)
  end
end
