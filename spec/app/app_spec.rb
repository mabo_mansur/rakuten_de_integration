RSpec.describe App do
  include_context 'publisher_mock'

  describe 'GET root path', vcr: true do
    it 'should return 404 status' do
      get '/'
      expect(last_response.status).to eq(404)
    end
  end

  describe 'GET sign up with params', vcr: true do
    let!(:params) { {user_token: 'veeqo_user_token', channel_token: 'veeqo_channel_token'} }

    it 'renders sign up form' do
      get '/signup', params
      expect(last_response).to be_ok

      expect(last_response.body).to include('Sign up')
    end
  end

  describe 'GET sign up without params', vcr: true do
    it 'renders sign up form' do
      get '/signup', user_token: ''
      expect(last_response.body).to include('Registration failed')
      expect(last_response.body).to include('error_messages: You\'re not authorized')
    end
  end

  describe 'POST registration with all params', vcr: true, sidekiq: :inline do
    before { allow(Orders::InitSynchronizerWorker).to receive(:perform_in).and_return(true) }

    let!(:params) do
      {
        rakuten_auth_key: SANDBOX_RAKUTEN_API_KEY,
        veeqo_user_token: 'veeqo_user_token',
        veeqo_channel_token: 'veeqo_channel_token'
      }
    end

    it 'renders sign up form' do
      post '/registration', params
      expect(last_response.location).to eq(ENV['VEEQO_REGISTRATION_REDIRECT_URL'])
      expect(Orders::InitSynchronizerWorker).to have_received(:perform_in).once
    end
  end
end
