RSpec.describe VeeqoChannel, type: :service, vcr: true, sidekiq: :inline do
  include_context 'publisher_mock'

  let(:reason) { 'Some reason' }
  let(:message) { 'Some message' }
  let(:queue) { @bunny_mock.channel.queue('rakuten.activation.callbacks') }

  describe 'activate' do
    let(:user) { create(:user, veeqo_channel_active: false) }
    let(:service) { VeeqoChannel.new(user) }

    it 'by integration' do
      expect { service.activate_by_integration(reason, message) }.to change { queue.message_count }.by(1)
      pushed_message = JSON.parse(queue.pop[2]).deep_symbolize_keys!

      expect(user.reload.veeqo_channel_active).to be true
      expect(pushed_message[:user_token]).to eq(user.veeqo_user_token)
      expect(pushed_message[:payload][:active]).to be true
      expect(pushed_message[:payload][:activation_reason_code]).to eq(reason)
      expect(pushed_message[:payload][:activation_message]).to eq(message)
    end

    it 'by veeqo' do
      expect { service.activate_by_veeqo }.to_not change { queue.message_count }

      expect(user.reload.veeqo_channel_active).to be true
    end
  end

  describe 'deactivate' do
    let(:user) { create(:user, veeqo_channel_active: true) }
    let(:service) { VeeqoChannel.new(user) }

    it 'by integration' do
      expect { service.deactivate_by_integration(reason, message) }.to change { queue.message_count }.by(1)
      pushed_message = JSON.parse(queue.pop[2]).deep_symbolize_keys!

      expect(user.reload.veeqo_channel_active).to be false
      expect(pushed_message[:user_token]).to eq(user.veeqo_user_token)
      expect(pushed_message[:payload][:active]).to be false
      expect(pushed_message[:payload][:deactivation_reason_code]).to eq(reason)
      expect(pushed_message[:payload][:deactivation_message]).to eq(message)
    end

    it 'by veeqo' do
      expect { service.deactivate_by_veeqo }.to_not change { queue.message_count }

      expect(user.reload.veeqo_channel_active).to be false
    end
  end

  describe '.apply_veeqo_settings' do
    let(:user) { create(:user) }
    let(:service) { VeeqoChannel.new(user) }
    let(:settings) { {veeqo_channel_active: false} }

    it 'success' do
      expect { service.apply_veeqo_settings(settings) }.to_not change { queue.message_count }

      expect(user.reload.veeqo_channel_active).to be false
    end
  end
end
