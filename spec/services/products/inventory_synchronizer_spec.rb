RSpec.describe Products::InventorySynchronizer, '#synchronize', vcr: true do
  let!(:user) { create :user, rakuten_auth_key: '3704e46f3ff0ddf91a7e9d0e35b2bf0c' }
  let!(:serv_double) { double }

  before { allow(Publisher).to receive(:publish).and_return(true) }

  context 'with valid payload' do
    before do
      allow(Rakuten::Client).to receive(:new).with(user).and_return(serv_double)
      allow(serv_double).to receive(:edit_product_variant)
      allow(serv_double).to receive(:edit_product)

      described_class.new(channel_message).synchronize
    end

    context 'update rakuten product' do
      let(:channel_message) do
        {
          user_token: user.veeqo_user_token,
          channel_token: user.veeqo_channel_token,
          payload: {
            remote_product_id: '2111607350',
            remote_variant_id: '2111607350',
            action: 'update_stock_level',
            quantity: 99,
            infinite: false,
            inventory_tracking: true
          }
        }
      end

      it 'should success' do
        expect(serv_double).to have_received(:edit_product).once
      end
    end

    context 'update rakuten variant' do
      let(:channel_message) do
        {
          user_token: user.veeqo_user_token,
          channel_token: user.veeqo_channel_token,
          payload: {
            remote_product_id: '2183083355',
            remote_variant_id: '2889496670',
            action: 'update_stock_level',
            quantity: 99,
            infinite: false,
            inventory_tracking: true
          }
        }
      end

      it 'should success' do
        expect(serv_double).to have_received(:edit_product_variant).once
      end
    end
  end

  context 'when update rakuten product with invalid payload' do
    let(:channel_message) do
      {
        user_token: user.veeqo_user_token,
        channel_token: user.veeqo_channel_token,
        payload: {
          remote_product_id: '',
          remote_variant_id: '',
          action: 'update_stock_level',
          quantity: 99,
          infinite: false,
          inventory_tracking: true
        }
      }
    end

    subject { described_class.new(channel_message).synchronize }

    it 'should fail' do
      expect { subject }.to raise_error(Rakuten::Errors::ApiError, '#41, Der Parameter "product_id" besitzt einen ungültigen Wert')
    end
  end
end
