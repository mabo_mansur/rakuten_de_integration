RSpec.describe Products::CollectionSynchronizer, sidekiq: :inline do
  let!(:user) { create :user }
  let!(:products_data) { RakutenFixtureData::Products.one_page[:products][:product] }
  let!(:product_not_changed) { create :product, user_id: user.id, rakuten_id: '300', data: products_data[0] }
  let!(:product_changed) { create :product, user_id: user.id, rakuten_id: '301', data: nil }

  before { allow(Products::EntitySynchronizerWorker).to receive(:perform_async) }

  subject { described_class.new(user, 1).synchronize }

  context 'collect rakuten products when have products' do
    before do
      allow_any_instance_of(Rakuten::API).to receive(:get_products).with(per_page: 100, page: 1).and_return(RakutenFixtureData::Products.one_page)
      allow(Publisher).to receive(:publish).and_return(true)
      subject
    end

    it 'Check Entity Synchronizer jobs' do
      expect(Products::EntitySynchronizerWorker).to have_received(:perform_async).with(user.id, products_data[0]).exactly(0)
      expect(Products::EntitySynchronizerWorker).to have_received(:perform_async).with(user.id, products_data[1]).once
      expect(Products::EntitySynchronizerWorker).to have_received(:perform_async).with(user.id, products_data[2]).once
    end
  end

  context 'collect rakuten products when have no products' do
    before do
      allow_any_instance_of(Rakuten::API).to receive(:get_products).with(per_page: 100, page: 1).and_return(RakutenFixtureData::Products.product_empty)
      allow(Publisher).to receive(:publish).and_return(true)
      subject
    end

    it 'Check Entity Synchronizer jobs' do
      expect(Products::EntitySynchronizerWorker).to have_received(:perform_async).exactly(0)
    end
  end
end
