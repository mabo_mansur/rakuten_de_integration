RSpec.describe Products::InitSynchronizer do
  let(:user) { create :user }

  before { allow(Products::CollectionSynchronizerWorker).to receive(:perform_async) }
  subject { described_class.new(user).synchronize }

  context 'collect rakuten products' do
    before do
      allow_any_instance_of(Rakuten::API).to receive(:get_products).with(per_page: 100, page: 1).and_return(RakutenFixtureData::Products.all(1))
      allow_any_instance_of(Rakuten::API).to receive(:get_products).with(per_page: 100, page: 2).and_return(RakutenFixtureData::Products.all(2))
      allow_any_instance_of(Rakuten::API).to receive(:get_products).with(per_page: 100, page: 3).and_return(RakutenFixtureData::Products.all(3))
      allow_any_instance_of(Products::EntitySynchronizerWorker).to receive(:perform).and_return(true)
      allow(Publisher).to receive(:publish).and_return(true)

      subject
    end

    it 'with valid response from rakuten when many products' do
      expect(Products::CollectionSynchronizerWorker).to have_received(:perform_async).exactly(2)
    end
  end

  context 'collect rakuten products when has no products' do
    before do
      allow_any_instance_of(Rakuten::API).to receive(:get_products).with(per_page: 100, page: 1).and_return(RakutenFixtureData::Products.product_empty)

      allow_any_instance_of(Products::EntitySynchronizerWorker).to receive(:perform).and_return(true)
      allow(Publisher).to receive(:publish).and_return(true)

      subject
    end

    it 'with valid response from rakuten when many products' do
      expect(Products::CollectionSynchronizerWorker).to have_received(:perform_async).exactly(0)
    end
  end
end
