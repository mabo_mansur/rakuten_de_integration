RSpec.describe Products::EntitySynchronizer do
  let!(:user) { create :user }
  let!(:product_hash) { RakutenFixtureData::Products.product }

  before { allow(Publisher).to receive(:publish).and_return(true) }

  context 'when new product' do
    it 'Check Entity Synchronizer jobs' do
      described_class.new(user, product_hash).synchronize
      expect(user.products.count).to eq(1)
      expect(user.products.last.synchronized).to eq(true)
      expect(user.products.last.data.deep_symbolize_keys).to eq(product_hash)
    end
  end

  context 'when already have product with no changes' do
    let!(:product_not_changed) { create :product, user_id: user.id, rakuten_id: '299', data: product_hash }

    it 'Check Entity Synchronizer jobs' do
      described_class.new(user, product_hash).synchronize
      expect(user.products.count).to eq(1)
      expect(user.products.last.synchronized).to eq(true)
      expect(user.products.last.data.deep_symbolize_keys).to eq(product_hash)
    end
  end

  context 'when already have product but with remote changes' do
    let!(:product_changed) { create :product, user_id: user.id, rakuten_id: '299', data: {test: 123} }

    it 'Check Entity Synchronizer jobs' do
      described_class.new(user, product_hash).synchronize
      expect(user.products.count).to eq(1)
      expect(user.products.last.synchronized).to eq(true)
      expect(user.products.last.data.deep_symbolize_keys).to eq(product_hash)
    end
  end
end
