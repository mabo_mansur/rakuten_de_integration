describe Rakuten::Client, '#orders' do
  let!(:user) { create :user, veeqo_user_token: 'asdasdasd', veeqo_channel_token: '123123', veeqo_channel_active: true }

  subject { Rakuten::Client.new(user).orders('payout', 1) }

  context 'when user deactivated' do
    let!(:deactivated_user) { create :user, veeqo_user_token: 'd_user_token', veeqo_channel_token: 'd_channel_token', veeqo_channel_active: false }

    it 'should raise error' do
      expect { Rakuten::Client.new(deactivated_user).orders('payout', 1) }.to raise_error(Rakuten::Errors::DeactivatedUser, "##{deactivated_user.id} User Deactivated!")
    end
  end

  context 'when successful response' do
    before { allow_any_instance_of(Rakuten::API).to receive(:get_orders).with(per_page: 100, page: 1, status: 'payout').and_return(RakutenFixtureData::Orders.processing_one) }

    it 'should return entities data' do
      expect(subject[:success]).to eq('1')
      expect(subject[:orders][:order][:order_no]).to eq('100-000-000')
    end
  end

  context 'when unsuccessful response' do
    context 'unexpected error' do
      before { allow_any_instance_of(Rakuten::API).to receive(:get_orders).with(per_page: 100, page: 1, status: 'payout').and_return(RakutenFixtureData::Orders.unexpected_error) }

      it 'should raise error' do
        expect { subject }.to raise_error(Rakuten::Errors::ApiError, '#9999, Unexpected Error Text')
      end
    end

    context 'many codes error' do
      before { allow_any_instance_of(Rakuten::API).to receive(:get_orders).with(per_page: 100, page: 1, status: 'payout').and_return(RakutenFixtureData::Orders.many_codes_error) }

      it 'should raise error' do
        expect { subject }.to raise_error(Rakuten::Errors::ApiError, '#40, Der benötigte Parameter \'name\' konnte nicht gefunden werden')
      end
    end

    context 'unauthorised error' do
      before do
        allow_any_instance_of(Rakuten::API).to receive(:get_orders).with(per_page: 100, page: 1, status: 'payout').and_return(RakutenFixtureData::Orders.unauthorised)
        allow(PublisherWorker).to receive(:perform_async)
      end

      it 'should raise exception' do
        expect { subject }.to raise_error(Rakuten::Errors::Unauthorised, 'Unauthorised Error')
        expect(user.reload.veeqo_channel_active).to be(false)

        msg = {
          user_token: 'asdasdasd',
          channel_token: '123123',
          payload:  {
            active: false,
            deactivation_reason_code: 'Unauthorised',
            deactivation_message: '#16, Der Händler-Schlüssel konnte nicht gefunden werden'
          }
        }

        expect(PublisherWorker).to have_received(:perform_async).with('rakuten.activation.callbacks', msg).once
      end
    end
  end
end
