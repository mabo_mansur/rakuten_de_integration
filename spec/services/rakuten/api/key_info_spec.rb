RSpec.describe Rakuten::API, '#key_info', vcr: true do
  context 'get request info from remote server with valid params' do
    it 'should return success response' do
      data = Rakuten::API.new('123456789a123456789a123456789a12').key_info
      expect(data[:success]).to eq('1')
      expect(data[:key][:active]).to eq('1')
      expect(data[:key][:calls_today]).to eq('20')
      expect(data[:key][:last_call_datetimestamp]).to eq('2010-01-01 20:15:00')
      expect(data[:key][:daily_calls_limit]).to eq('10000')
      expect(data[:key][:permissions]).to eq(products: 'write', orders: 'read', categories: 'forbidden')
    end
  end

  context 'get request info from remote server with invalid key' do
    it 'should return bad response' do
      data = Rakuten::API.new('invalid_key').key_info
      expect(data[:success]).to eq('-1')
      expect(data[:errors][:error]).to eq(code: '15', message: 'Der Händler-Schlüssel ist ungültig', help: 'http://webservice.rakuten.de/documentation/howto/auth')
    end
  end

  context 'get request info from remote server without key' do
    it 'should return bad response' do
      data = Rakuten::API.new('invalid_key').key_info
      expect(data[:success]).to eq('-1')
      expect(data[:errors][:error]).to eq(code: '15', message: 'Der Händler-Schlüssel ist ungültig', help: 'http://webservice.rakuten.de/documentation/howto/auth')
    end
  end
end
