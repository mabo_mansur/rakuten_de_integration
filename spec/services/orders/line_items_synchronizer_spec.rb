RSpec.describe Orders::LineItemsSynchronizer, '.synchronize', type: :service, vcr: true, sidekiq: :inline do
  let!(:user) { create :user }
  let!(:order) { create :order, user_id: user.id, rakuten_id: '100-000-000', synchronized: true, data: order_data }

  before { allow(Products::EntitySynchronizerWorker).to receive(:perform_async).and_return(true) }

  subject { Orders::LineItemsSynchronizer.new(user, order).synchronize }

  context 'with success fetch' do
    context 'when one line item' do
      let!(:order_data) { RakutenFixtureData::Orders.processing_hash }

      context 'and product with data exist in db' do
        let!(:product_data) { RakutenFixtureData::Products.product }

        before do
          product_data[:product_id] = order_data['items']['item']['product_id']
          product_data[:variants][:variant][:variant_id] = order_data['items']['item']['variant_id']
          product_data[:name] = 'Musterprodukt product data'
          Product.create(rakuten_id: 4, synchronized: false, user_id: user.id, data: product_data)
          subject
        end

        it { expect(Products::EntitySynchronizerWorker).to have_received(:perform_async).with(user.id, product_data.deep_stringify_keys).once }
      end

      context 'and product exist in db without data' do
        let!(:serv_double) { double }
        let!(:product_response) { RakutenFixtureData::Products.one }

        before do
          Product.create(rakuten_id: 4, synchronized: false, user_id: user.id, data: nil)
          allow(Rakuten::Client).to receive(:new).with(user).and_return(serv_double)
          allow(serv_double).to receive(:product).with('4').and_return(product_response)
          subject
        end

        it { expect(Products::EntitySynchronizerWorker).to have_received(:perform_async).with(user.id, product_response[:products][:product]).once }
      end
    end

    context 'when some line items' do
      let!(:order_data) { RakutenFixtureData::Orders.extended_processing_hash }
      let!(:product_data) { RakutenFixtureData::Products.product }

      before do
        product_data[:product_id] = order_data['items']['item'].first['product_id']
        product_data[:variants][:variant][:variant_id] = order_data['items']['item'].first['variant_id']
        product_data[:name] = 'Musterprodukt array product data'

        Product.create(rakuten_id: order_data['items']['item'].first['product_id'], synchronized: false, user_id: user.id, data: product_data)
        subject
      end

      it 'should run workers as much as order line items count' do
        expect(Products::EntitySynchronizerWorker).to have_received(:perform_async).with(user.id, product_data.deep_stringify_keys).once
        expect(Products::EntitySynchronizerWorker).to have_received(:perform_async).exactly(6)
      end
    end
  end

  context 'with fail fetch' do
    let!(:order_data) { RakutenFixtureData::Orders.processing_hash }

    context 'when raise unauthorised error' do
      before do
        allow_any_instance_of(Rakuten::API).to(
          receive(:get_products).
          with(search_field: 'product_id', search: '4').
          and_return(RakutenFixtureData::Orders.unauthorised)
        )
      end

      it { expect { subject }.to raise_error(Rakuten::Errors::Unauthorised, 'Unauthorised Error') }
    end

    context 'when not found product id in external api' do
      before do
        empry_response = {
          success: 1,
          products: {
            paging: {total: '0', page: '1', pages: '0', per_page: '20'}
          }
        }

        allow_any_instance_of(Rakuten::API).to(
          receive(:get_products).
          with(search_field: 'product_id', search: '4').
          and_return(empry_response)
        )
      end

      it { expect { subject }.to raise_error(Errors::FetchError, 'Product data is nil') }
    end
  end
end
