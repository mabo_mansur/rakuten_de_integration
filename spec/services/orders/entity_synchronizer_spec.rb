RSpec.describe Orders::EntitySynchronizer, '#synchronize', type: :service, vcr: true, sidekiq: :inline do
  let!(:user) { create(:user) }
  let!(:order_hash) { RakutenFixtureData::Orders.processing_hash }

  before { allow(Publisher).to receive(:publish).and_return(true) }

  subject { Orders::EntitySynchronizer.new(user, order_hash).synchronize }

  context 'when sync new order' do
    before { subject }

    it 'should create order in db and publish to rabbitmq' do
      expect(Order.where(user_id: user.id, rakuten_id: '100-000-000', synchronized: true).count).to eq(1)
      expect(Order.last.data).to eq(order_hash)
      expect(Publisher).to have_received(:publish).once
    end
  end

  context 'when sync exist order' do
    before do
      Order.create(user_id: user.id, rakuten_id: '100-000-000', synchronized: true, data: {test: 123})
      subject
    end

    it 'should not create in db and not publish' do
      expect(Order.count).to eq(1)
      expect(Order.last.data).to eq('test' => 123)
      expect(Publisher).to have_received(:publish).exactly(0)
    end
  end
end
