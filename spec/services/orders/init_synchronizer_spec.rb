RSpec.describe Orders::InitSynchronizer, '#synchronize', type: :service do
  let!(:user) { create(:user) }

  before do
    allow(Orders::EntitySynchronizerWorker).to receive(:perform_async)
    allow(Orders::CollectionSynchronizerWorker).to receive(:perform_async)
  end

  subject { Orders::InitSynchronizer.new(user).synchronize }

  context 'with valid response from rakuten when many orders' do
    before do
      allow_any_instance_of(Rakuten::API).to receive(:get_orders).with(per_page: 100, page: 1, status: 'editable').and_return(RakutenFixtureData::Orders.processing(1))
      allow_any_instance_of(Rakuten::API).to receive(:get_orders).with(per_page: 100, page: 2, status: 'editable').and_return(RakutenFixtureData::Orders.processing(2))
      allow_any_instance_of(Rakuten::API).to receive(:get_orders).with(per_page: 100, page: 3, status: 'editable').and_return(RakutenFixtureData::Orders.processing(3))
      create(:order, user_id: user.id, rakuten_id: '100-000-070', synchronized: true)
      create(:order, user_id: user.id, rakuten_id: '100-000-080', synchronized: true)
      create(:order, user_id: user.id, rakuten_id: '100-000-100', synchronized: true)
      subject
    end

    it { expect(Orders::EntitySynchronizerWorker).to have_received(:perform_async).exactly(98) }
    it { expect(Orders::CollectionSynchronizerWorker).to have_received(:perform_async).exactly(2) }
  end

  context 'with valid response from rakuten when one order' do
    before do
      allow_any_instance_of(Rakuten::API).to receive(:get_orders).with(per_page: 100, page: 1, status: 'editable').and_return(RakutenFixtureData::Orders.processing_one)
      subject
    end

    it { expect(Orders::EntitySynchronizerWorker).to have_received(:perform_async).once }
    it { expect(Orders::CollectionSynchronizerWorker).to have_received(:perform_async).exactly(0) }
  end

  context 'with valid response from rakuten when one synced order' do
    before do
      allow_any_instance_of(Rakuten::API).to receive(:get_orders).with(per_page: 100, page: 1, status: 'editable').and_return(RakutenFixtureData::Orders.processing_one)
      create(:order, user_id: user.id, rakuten_id: '100-000-000', synchronized: true)
      subject
    end

    it { expect(Orders::EntitySynchronizerWorker).to have_received(:perform_async).exactly(0) }
    it { expect(Orders::CollectionSynchronizerWorker).to have_received(:perform_async).exactly(0) }
  end

  context 'with valid response from rakuten when no orders' do
    before do
      allow_any_instance_of(Rakuten::API).to receive(:get_orders).with(per_page: 100, page: 1, status: 'editable').and_return(RakutenFixtureData::Orders.empty)
      subject
    end

    it { expect(Orders::CollectionSynchronizerWorker).to have_received(:perform_async).exactly(0) }
  end
end
