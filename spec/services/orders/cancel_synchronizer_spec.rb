RSpec.describe Orders::CancelSynchronizer, type: :service, vcr: true, sidekiq: :inline do
  include_context 'publisher_mock'

  let!(:serv_double) { double }
  let!(:user) { create(:user) }
  let!(:channel_message) do
    {
      user_token: 'user_token',
      channel_token: 'channel_token',
      payload: {
        remote_order_id: '123-456-789',
        action: 'cancel',
        cancelled_at: '2017-11-23 16:30 UTC',
        cancel_reason: 'Product is out of stock, sorry',
        notify_client: false
      }
    }
  end

  before do
    allow(Rakuten::API).to receive(:new).and_return(serv_double)
    allow(serv_double).to receive(:set_order_cancelled).with(order_no: '123-456-789', comment: 'Product is out of stock, sorry').and_return(success: '1')
    Orders::CancelSynchronizer.new(channel_message).to_rakuten
  end

  describe '.to_rakuten' do
    it 'makes cancel for order on Rakuten' do
      expect(serv_double).to have_received(:set_order_cancelled).once
    end
  end
end
