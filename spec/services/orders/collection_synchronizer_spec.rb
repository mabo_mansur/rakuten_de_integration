RSpec.describe Orders::CollectionSynchronizer, '#synchronize', type: :service do
  let!(:user) { create(:user) }

  before { allow(Orders::EntitySynchronizerWorker).to receive(:perform_async) }

  context 'with valid response from rakuten when many orders' do
    before do
      allow_any_instance_of(Rakuten::API).to receive(:get_orders).with(per_page: 100, page: 1, status: 'editable').and_return(RakutenFixtureData::Orders.processing(1))
      create(:order, user_id: user.id, rakuten_id: '100-000-030', synchronized: false)
      create(:order, user_id: user.id, rakuten_id: '100-000-070', synchronized: true)
      create(:order, user_id: user.id, rakuten_id: '100-000-080', synchronized: true)
      Orders::CollectionSynchronizer.new(user, 1).synchronize
    end

    it { expect(Orders::EntitySynchronizerWorker).to have_received(:perform_async).exactly(98) }
  end

  context 'with valid response from rakuten when one order' do
    before { allow_any_instance_of(Rakuten::API).to receive(:get_orders).with(per_page: 100, page: 3, status: 'editable').and_return(RakutenFixtureData::Orders.processing_one) }

    context 'not synced' do
      before { Orders::CollectionSynchronizer.new(user, 3).synchronize }

      it { expect(Orders::EntitySynchronizerWorker).to have_received(:perform_async).once }
    end

    context 'synced' do
      before do
        create(:order, user_id: user.id, rakuten_id: '100-000-000', synchronized: true)
        Orders::CollectionSynchronizer.new(user, 3).synchronize
      end

      it { expect(Orders::EntitySynchronizerWorker).to have_received(:perform_async).exactly(0) }
    end
  end
end
