RSpec.describe Orders::ShipSynchronizer, '#to_rakuten', type: :service, vcr: true, sidekiq: :inline do
  include_context 'publisher_mock'

  let!(:serv_double) { double }
  let!(:user) { create(:user) }

  before do
    allow(Rakuten::API).to receive(:new).and_return(serv_double)
    allow(serv_double).to receive(:set_order_shipped).with(shipped_params).and_return(success: '1')
    Orders::ShipSynchronizer.new(channel_message).to_rakuten
  end

  context 'ship order with full message' do
    let!(:channel_message) do
      {
        user_token: 'user_token',
        channel_token: 'channel_token',
        payload: {
          remote_order_id: '123-456-789',
          action: 'ship',
          shipments: [
            {
              carrier: 'Deutsche post einsCHreiben =)',
              carrier_service: '2nd day delivery',
              tracking_number: '111222',
              created_at: '2017-06-20 14:30 UTC'
            },
            {
              carrier: 'FedEx',
              carrier_service: '2nd day delivery',
              tracking_number: '33334444',
              created_at: '2017-06-20 14:31 UTC'
            }
          ]
        }
      }
    end
    let!(:shipped_params) { {order_no: '123-456-789', carrier: 'Deutsche Post (Einschreiben)', tracking_number: '111222'} }

    it { expect(serv_double).to have_received(:set_order_shipped).once }
  end

  context 'ship order without shipment' do
    let!(:channel_message) do
      {
        user_token: 'user_token',
        channel_token: 'channel_token',
        payload: {
          remote_order_id: '123-456-789',
          action: 'ship'
        }
      }
    end
    let!(:shipped_params) { {order_no: '123-456-789'} }

    it { expect(serv_double).to have_received(:set_order_shipped).once }
  end

  context 'ship order without carrier' do
    let!(:channel_message) do
      {
        user_token: 'user_token',
        channel_token: 'channel_token',
        payload: {
          remote_order_id: '123-456-789',
          action: 'ship',
          shipments: [
            {
              carrier: '',
              carrier_service: '2nd day delivery',
              tracking_number: '111222',
              created_at: '2017-06-20 14:30 UTC'
            }
          ]
        }
      }
    end
    let!(:shipped_params) { {order_no: '123-456-789'} }

    it { expect(serv_double).to have_received(:set_order_shipped).once }
  end

  context 'ship order without tracking_number' do
    let!(:channel_message) do
      {
        user_token: 'user_token',
        channel_token: 'channel_token',
        payload: {
          remote_order_id: '123-456-789',
          action: 'ship',
          shipments: [
            {
              carrier: 'post.at',
              carrier_service: '',
              tracking_number: '',
              created_at: ''
            }
          ]
        }
      }
    end
    let!(:shipped_params) { {order_no: '123-456-789', carrier: 'Post.at', tracking_number: ''} }

    it { expect(serv_double).to have_received(:set_order_shipped).once }
  end
end
