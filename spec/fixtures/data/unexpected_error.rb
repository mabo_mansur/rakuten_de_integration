{
  success: '-1',
  errors: {
    error: {
      code: '9999',
      message: 'Unexpected Error Text',
      help: 'http://webservice.rakuten.de/documentation/howto/auth'
    }
  }
}
