{
  success: '-1',
  errors: {
    error: {
      code: '16',
      message: 'Der Händler-Schlüssel konnte nicht gefunden werden',
      help: 'http://webservice.rakuten.de/documentation/howto/auth'
    }
  }
}
