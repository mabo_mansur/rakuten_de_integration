{
  product_id: 299,
  product_art_no: 'ART-119',
  name: 'Musterprodukt 119',
  producer: 'Musterhersteller',
  shipping_group: '1',
  tax: '1',
  min_order_qty: '1',
  staggering: '9,18,27',
  stock_policy: '1',
  visible: '1',
  homepage: '0',
  connect: '1',
  description: 'Tolles Produkt!',
  inci: 'Inhaltsangabe',
  comment: 'Verkauft sich gut',
  cross_selling_title: 'Diese Produkte sind auch super!',
  created: '2010-01-01 20:15:00',
  url_portal: 'http:\\/\\/www.rakuten.de\\/p\\/mein-produkt-123456',
  url_shop: 'http:\\/\\/www.domain.de\\/p\\/123456\\/mein-produkt',
  url_marketing: 'http:\\/\\/www.domain.de\\/sp\\/mein-produkt-123456.html',
  has_variants: '1',
  images: {
    image: {
      image_id: '2',
      src: 'URL zum Bild',
      default: '1',
      commnet: 'Musterprodukt im Wohnzimmer'
    }
  },
  variants: {
    label: 'Farben',
    variant: {
      variant_id: '5',
      variant_art_no: 'VART-88',
      name: 'Blau',
      price: '1.99',
      price_reduced: '0.99',
      price_reduced_type: 'VK',
      baseprice_unit: 'ml',
      baseprice_volume: '0.750',
      stock: '20',
      delivery: '3',
      available: '1',
      isbn: '123456789',
      ean: '123456789',
      mpn: '123456789',
      sort: '2'
    }
  }
}
