{
  success: '1',
  orders: {
    paging: {total: '1', page: '1', pages: '1', per_page: '100'},
    order: {
      order_no: '100-000-000',
      total: '12.12',
      shipping: '3.50',
      max_shipping_date: '2010-01-01 20:15:00',
      payment: 'CC',
      status: 'payout',
      invoice_no: '123456',
      comment_client: 'Ich freu mich so sehr!',
      comment_merchant: 'Beim Lieferanten bestellt',
      created: '2010-01-01 20:15:00',
      client: {
        client_id: '1',
        gender: 'Herr',
        first_name: 'Max',
        last_name: 'Mustermann',
        company: 'Muster GmbH',
        street: 'Musterstra\u00dfe',
        street_no: '1',
        address_add: 'Seiteneingang',
        zip_code: '11111',
        city: 'Musterstadt',
        country: 'DE',
        email: 'max@mustermann.de',
        phone: '123456-4555'
      },
      delivery_address: {
        gender: 'D Herr',
        first_name: 'D Max',
        last_name: 'D Mustermann',
        company: 'D Muster GmbH',
        street: 'D Musterstra\u00dfe',
        street_no: 'D 1',
        address_add: 'D Seiteneingang',
        zip_code: 'D 11111',
        city: 'D Musterstadt',
        country: 'D DE'
      },
      items: {
        item: {
          item_id: '1',
          product_id: '4',
          variant_id: '5',
          product_art_no: 'ART-99',
          name: 'Musterprodukt',
          name_add: 'Gr\u00fcn',
          qty: '2',
          price: '10.00',
          price_sum: '20.00',
          tax: '1'
        }
      },
      coupon: {
        coupon_id: '1',
        total: '10.00',
        code: 'ABCDEFG',
        comment: 'Neukunde'
      }
    }
  }
}
