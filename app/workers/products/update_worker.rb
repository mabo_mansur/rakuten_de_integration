module Products
  class UpdateWorker
    include Sidekiq::Worker
    sidekiq_options queue: :update_products, retry: 12

    def perform(channel_message)
      message = Oj.load(channel_message, symbol_keys: true)
      return if message[:payload][:action] != 'update_stock_level'

      Products::InventorySynchronizer.new(message).synchronize
    end
  end
end
