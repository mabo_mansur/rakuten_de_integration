module Products
  class InitSynchronizerWorker
    include Sidekiq::Worker
    sidekiq_options queue: :pull_products, retry: 12

    def perform(user_id)
      user = User.find(user_id)
      Products::InitSynchronizer.new(user).synchronize
    end
  end
end
