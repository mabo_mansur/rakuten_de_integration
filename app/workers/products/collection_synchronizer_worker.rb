module Products
  class CollectionSynchronizerWorker
    include Sidekiq::Worker
    sidekiq_options queue: :pull_products, retry: 12

    def perform(user_id, page)
      user = User.find(user_id)
      Products::CollectionSynchronizer.new(user, page).synchronize
    end
  end
end
