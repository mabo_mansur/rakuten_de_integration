module Products
  class EntitySynchronizerWorker
    include Sidekiq::Worker
    sidekiq_options queue: :push_products, retry: 12

    sidekiq_retries_exhausted do |msg, e|
      user = User.find(msg['args'].first)
      Producers::Fetching.new(user).failed('products', e.class.name, e.message)
    end

    def perform(user_id, product_hash)
      user = User.find(user_id)
      Products::EntitySynchronizer.new(user, product_hash).synchronize
    end
  end
end
