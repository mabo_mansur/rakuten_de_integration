module Consumer
  class ProductsUpdate
    include Sneakers::Worker
    from_queue 'rakuten.products.update', env: nil

    def work(msg)
      Products::UpdateWorker.perform_async(msg)
      Sneakers.logger.info("Raw data: ##{msg}")
      ack!
    end
  end
end
