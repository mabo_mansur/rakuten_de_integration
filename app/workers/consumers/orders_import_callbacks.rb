module Consumer
  class OrdersImportCallbacks
    include Sneakers::Worker
    from_queue 'rakuten.orders.import.callbacks', env: nil

    def work(msg)
      Orders::ImportCallbacksWorker.perform_async(msg)
      Sneakers.logger.info("Raw data: ##{msg}")
      ack!
    end
  end
end
