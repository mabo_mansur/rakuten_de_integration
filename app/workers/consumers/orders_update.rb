module Consumer
  class OrdersUpdate
    include Sneakers::Worker
    from_queue 'rakuten.orders.update', env: nil

    def work(msg)
      Orders::UpdateWorker.perform_async(msg)
      Sneakers.logger.info("Raw data: ##{msg}")
      ack!
    end
  end
end
