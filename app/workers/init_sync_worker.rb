class InitSyncWorker
  include Sidekiq::Worker
  include LogHelper
  sidekiq_options retry: false

  # running after user finish registration
  def perform(user_id)
    user = User.find(user_id)
    tag_logger("User ##{user.id}", 'Init Sync')

    log(:info, 'Start init sync products')
    Products::InitSynchronizerWorker.perform_async(user.id)
    log(:info, 'Start init sync orders')
    Orders::InitSynchronizerWorker.perform_in(10.minutes, user.id)
  rescue StandardError => e
    tag_logger("User ##{user.id}", 'Init Sync error')
    log(:error, "Error init sync from Rakuten to Veeqo: #{e.message}")
    log(:error, "Backtrace: #{e.backtrace.join("\n")}")
    raise e
  end
end
