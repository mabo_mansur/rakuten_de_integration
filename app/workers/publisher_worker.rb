class PublisherWorker
  include Sidekiq::Worker
  sidekiq_options queue: :publish_messages, retry: 12

  def perform(queue, message)
    Publisher.publish(queue, message)
  end
end
