module Orders
  class InitSynchronizerWorker
    include Sidekiq::Worker
    sidekiq_options queue: :pull_orders, retry: 12

    def perform(user_id)
      user = User.find(user_id)
      Orders::InitSynchronizer.new(user).synchronize
    end
  end
end
