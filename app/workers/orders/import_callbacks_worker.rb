module Orders
  class ImportCallbacksWorker
    include Sidekiq::Worker
    include LogHelper
    sidekiq_options retry: 12

    def perform(channel_message)
      tag_logger('Orders::ImportCallbacksWorker')
      log(:info, channel_message)

      channel_message = Oj.load(channel_message, symbol_keys: true)
      user = User.find_by!(veeqo_channel_token: channel_message[:channel_token])
      order = user.orders.find_by!(rakuten_id: channel_message[:payload][:remote_order_id])

      if channel_message[:payload][:message] == 'contains_not_pulled_products'
        Orders::LineItemsSynchronizer.new(user, order).synchronize
      end
    end
  end
end
