module Orders
  class EntitySynchronizerWorker
    include Sidekiq::Worker
    sidekiq_options queue: :push_orders, retry: 12

    sidekiq_retries_exhausted do |msg, e|
      user = User.find(msg['args'].first)
      Producers::Fetching.new(user).failed('orders', e.class.name, e.message)
    end

    def perform(user_id, order_hash)
      user = User.find(user_id)
      Orders::EntitySynchronizer.new(user, order_hash).synchronize
    end
  end
end
