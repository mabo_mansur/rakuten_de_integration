module Orders
  class UpdateWorker
    include Sidekiq::Worker
    sidekiq_options queue: :update_orders, retry: 12

    def perform(channel_message)
      channel_message = Oj.load(channel_message, symbol_keys: true)

      case channel_message[:payload][:action]
      when 'cancel'
        Orders::CancelSynchronizer.new(channel_message).to_rakuten
      when 'ship'
        Orders::ShipSynchronizer.new(channel_message).to_rakuten
      end
    end
  end
end
