module Orders
  class CollectionSynchronizerWorker
    include Sidekiq::Worker
    sidekiq_options queue: :pull_orders, retry: 12

    def perform(user_id, page)
      user = User.find(user_id)
      Orders::CollectionSynchronizer.new(user, page).synchronize
    end
  end
end
