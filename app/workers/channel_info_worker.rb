class ChannelInfoWorker
  include Sidekiq::Worker
  sidekiq_options retry: 12

  def perform(channel_message)
    channel_message = Oj.load(channel_message, symbol_keys: true)
    info = channel_message[:payload][:channel]

    channel_state = info[:state] == 'active'

    settings = {veeqo_channel_active: channel_state}

    user = User.find_by!(veeqo_channel_token: channel_message[:channel_token])

    VeeqoChannel.new(user).apply_veeqo_settings(settings)
  end
end
