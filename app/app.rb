class App < Sinatra::Base
  include LogHelper

  register Sinatra::ConfigFile
  config_file '../config/config.yml'

  not_found do
    status 404
    erb :not_found
  end

  get '/signup' do
    tag_logger('GET', '/signup')
    log(:info, params.to_s)

    if validate_params[:status] == 'success'
      erb :'registration/signup', layout: :application
    else
      @errors = validate_params[:errors]
      erb :'registration/signup_error', layout: :application
    end
  end

  post '/registration' do
    tag_logger('POST', '/registration')
    log(:info, params.to_s)

    result = CreateUser.call(params)
    if result[:status] == 'success'
      user = result[:user]
      user.start_integration
      redirect ENV['VEEQO_REGISTRATION_REDIRECT_URL']
    else
      @errors = {error: result[:msg]}
      erb :'registration/signup_error', layout: :application
    end
  end

  run! if app_file == $PROGRAM_NAME

  private

  def validate_params
    response = VeeqoClient.new(params[:user_token]).validate_user
    response.success? ? {status: 'success'} : {errors: response.parsed_response}
  end
end
