class RakutenAuthenticateUser
  include InteractorHelper

  def self.call(*args)
    new(*args).call
  end

  def initialize(rakuten_auth_key)
    @rakuten_auth_key = rakuten_auth_key
  end

  def call
    response = rakuten_request
    if response[:success] == '-1'
      result('fail', handle_erros_message(response))
    else
      result('success')
    end
  end

  private

  def rakuten_request
    Rakuten::API.new(@rakuten_auth_key).key_info
  end

  def handle_erros_message(message)
    message[:errors][:error][:message]
  end
end
