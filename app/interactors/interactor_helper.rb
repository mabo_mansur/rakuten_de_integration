module InteractorHelper
  def result(status, msg = '')
    {status: status, msg: msg}
  end
end
