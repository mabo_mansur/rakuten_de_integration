class CreateUser
  include InteractorHelper
  include LogHelper

  def self.call(*args)
    new(*args).call
  end

  def initialize(params)
    @rakuten_auth_key = params.fetch(:rakuten_auth_key)
    @veeqo_user_token = params.fetch(:veeqo_user_token)
    @veeqo_channel_token = params.fetch(:veeqo_channel_token)
  end

  def call
    if authenticate_user[:status] == 'success'
      create_user
    else
      result('fail', authenticate_user[:msg])
    end
  end

  private

  def authenticate_user
    RakutenAuthenticateUser.call(@rakuten_auth_key)
  end

  def create_user
    user = User.create!(
      rakuten_auth_key: @rakuten_auth_key,
      veeqo_user_token: @veeqo_user_token,
      veeqo_channel_token: @veeqo_channel_token
    )
    {status: 'success', user: user}
  rescue StandardError => e
    tag_logger('User', 'create_user')
    log(:info, e.message)
    result('fail', 'User not created')
  end
end
