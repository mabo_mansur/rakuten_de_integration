class ProductSerializer
  include SerializerHelper

  attr_accessor :product, :variants, :options

  def initialize(product)
    @product = product
    @variants = find_variants
    @options = find_options
  end

  def prepare
    serialize_product(product).merge!(
      variants: serialize_variants,
      options: options
    )
  end

  def serialize_product(product)
    {
      id: product[:product_id],
      title: product[:name],
      description: product[:description],
      brand: product[:producer],
      upc_code: nil,
      sku_code: product[:product_art_no],
      taxes_included: true,
      tax_rate: tax_rate(product[:tax]),
      price_ex_tax: price_ex_taxes(product[:price], product[:tax]).to_s,
      price_inc_tax: product[:price].to_s,
      special_price_ex_tax: price_ex_taxes(product[:price_reduced], product[:tax]).to_s,
      special_price_inc_tax: product[:price_reduced].to_s,
      special_price_active_after: nil,
      currency: RAKUTEN_DEFAULT_CURRENCY,
      tag_list: nil,
      weight: nil,
      weight_unit: nil,
      width: nil,
      height: nil,
      length: nil,
      dimensions_unit: product[:baseprice_unit],
      available_quantity: product[:stock],
      inventory_tracking: inventory_tracking?,
      active: product_active?,
      images: serialize_images(product),
      additional_attributes: [],
      created_at: product[:created],
      updated_at: nil,
      deleted_at: nil
    }
  end

  def find_options
    return [] unless variants

    [*1..5].map do |i|
      name = variants[0][('variant' << i.to_s << '_type').to_sym]
      next unless name.present?
      {
        id: i.to_s,
        name: name,
        required: true
      }
    end.compact
  end

  def serialize_specifics(variant)
    [].tap do |specs|
      options.length.times do |i|
        i += 1
        specs << {
          id: i.to_s,
          option_id: i.to_s,
          value: variant[('variant' << i.to_s << '_value').to_sym]
        }
      end
    end
  end

  def find_variants
    v = product[:has_variants] == '1' ? product.delete(:variants) : nil
    return nil unless v

    ([] << v[:variant]).flatten
  end

  def inventory_tracking?
    product[:stock_policy].to_i == 1
  end

  def product_active?
    product[:visible].to_i == 1
  end

  def serialize_variants
    return [] unless variants

    variants.map do |variant|
      variant[:product_id] = variant.delete(:variant_id)
      variant[:product_art_no] = variant.delete(:variant_art_no)
      variant[:tax] = product[:tax]

      serialize_product(variant).merge!(
        specifics: serialize_specifics(variant)
      )
    end
  end

  def serialize_images(product)
    return [] unless product[:images] && product[:images][:image]
    images = product[:images][:image]
    images.is_a?(Hash) ? images = [] << images : images
    images.map do |image|
      {id: image[:image_id], url: image[:src]}
    end
  end
end
