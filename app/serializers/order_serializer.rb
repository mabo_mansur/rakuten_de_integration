class OrderSerializer
  include SerializerHelper

  attr_accessor :order

  def initialize(order)
    @order = order
  end

  def serialize
    {
      id: order[:order_no],
      number: order_number,
      due_date: order[:max_shipping_date],
      currency: RAKUTEN_DEFAULT_CURRENCY,
      status: transform_status[order[:status].to_sym],
      created_at: order[:created],
      updated_at: nil,
      cancelled_at: nil,
      cancel_reason: nil,
      shipped_at: nil,
      taxes_included: false,
      employee_notes: employee_notes(order[:comment_merchant]),
      customer_note: order[:comment_client],
      customer: customer,
      delivery_method: nil,
      shipping_address: shipping_address,
      billing_address: nil,
      payments: payments,
      line_items: line_items,
      totals: totals,
      shipments: []
    }
  end

  def order_number
    order[:invoice_no].present? ? order[:invoice_no] : order[:order_no]
  end

  def transform_status
    {
      pending: 'pending',
      editable: 'processing',
      shipped: 'shipped',
      payout: 'refunded',
      cancelled: 'cancelled'
    }
  end

  def employee_notes(note)
    [].tap { |a| a << {text: note} if note }
  end

  def customer
    customer = order[:client]
    return {} unless customer

    {
      id: customer[:client_id],
      email: customer[:email],
      first_name: customer[:first_name],
      second_name: customer[:last_name],
      phone: customer[:phone]
    }
  end

  def shipping_address
    address = order[:delivery_address]
    return {} unless address

    {
      id: nil,
      first_name: address[:first_name],
      last_name: address[:last_name],
      company: address[:company],
      address1: address[:street] + ' ' + address[:street_no],
      address2: address[:address_add],
      city: address[:city],
      country: address[:country],
      state: nil,
      zip: address[:zip_code],
      phone: nil,
      email: nil,
      created_at: nil,
      updated_at: nil
    }
  end

  def payments
    [{
      id: nil,
      payment_method: payment_method(order[:payment]),
      reference_number: nil
    }]
  end

  def line_items
    return [] unless order[:items]
    @line_items ||= ([] << order[:items][:item]).flatten.map do |item|
      {
        id: item[:item_id],
        product_id: item[:product_id],
        variant_id: line_items_variant_id(item),
        price_per_unit_inc_tax: nil,
        price_per_unit_ex_tax: price_ex_taxes(item[:price], item[:tax]).to_s,
        tax_rate: tax_rate(item[:tax]).to_s,
        quantity: item[:qty],
        discount_amount: '0.0',
        additional_options: []
      }
    end
  end

  def line_items_variant_id(item)
    item[:variant_id].to_i.zero? ? item[:product_id] : item[:variant_id]
  end

  def totals
    {
      adjustment_amount: 0,
      delivery_cost_inc_tax: nil,
      delivery_cost_ex_tax: delivery_cost_ex_tax.to_s,
      delivery_cost_tax_rate: delivery_tax_rate.to_s,
      delivery_cost_discount: delivery_cost_discount.to_s,
      order_discount: line_items_discount.to_s,
      total_discounts: total_discounts.to_s,
      total_price_inc_tax: order[:total].to_s, # total_price_ex_tax + total_tax - total_discount_ex_tax
      total_price_ex_tax: total_price_ex_tax.to_s,
      total_fees: '0.0',
      tax_rate: total_tax_rate.to_s,
      total_tax: total_tax.to_s
    }
  end

  def delivery_tax_rate
    subtotal = all_subtotals_sum - delivery_cost_discount - line_items_discount
    line_items_tax_amount / subtotal
  end

  def delivery_cost_discount
    0.0
  end

  def delivery_cost_ex_tax
    order[:shipping].to_d / (1 + delivery_tax_rate)
  end

  def delivery_cost_tax_amount
    order[:shipping].to_d - delivery_cost_ex_tax
  end

  def all_coupons_total_sum
    return 0.0 unless order[:coupon].present?
    ([] << order[:coupon]).flatten.map { |c| c[:total].to_d }.inject(:+)
  end

  def all_subtotals_sum
    line_items.map { |i| line_item_subtotal(i[:price_per_unit_ex_tax], i[:quantity]) }.inject(:+)
  end

  def line_items_discount
    return 0.0 if all_coupons_total_sum.zero?

    line_items.map do |i|
      subtotal = i[:price_per_unit_ex_tax].to_d * i[:quantity].to_i
      line_item_discount_inc_tax = subtotal / all_subtotals_sum * all_coupons_total_sum
      line_item_discount_inc_tax / (1 + i[:tax_rate].to_d) # line_item_discount_ex_tax
    end.inject(:+)
  end

  def discount_tax_amount
    all_coupons_total_sum - line_items_discount
  end

  def total_discounts
    order_discount + delivery_cost_discount + line_items_discount
  end

  def order_discount
    0.0
  end

  def total_price_ex_tax
    order[:total].to_d - total_tax
  end

  def total_tax_rate
    return '0' if total_price_ex_tax.zero?
    total_tax / total_price_ex_tax
  end

  def total_tax
    line_items_tax_amount + delivery_cost_tax_amount - discount_tax_amount
  end

  def line_items_tax_amount
    line_items.map do |i|
      line_item_subtotal(i[:price_per_unit_ex_tax], i[:quantity]) * i[:tax_rate].to_d
    end.inject(:+)
  end
end
