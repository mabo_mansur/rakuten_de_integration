module SerializerHelper
  RAKUTEN_DEFAULT_CURRENCY = 'EUR'.freeze
  TAX_FREE = 3
  TAXES = {
    1   => 19,
    2   => 7,
    TAX_FREE => 0,
    4   => 10.7,
    10  => 10,
    11  => 12,
    12  => 20,
    13  => 13
  }.freeze
  PAYMENT_TYPE = {
    'PP'     => 'Vorauskasse',
    'CC'     => 'Kreditkarte',
    'ELV'    => 'Lastschrift',
    'ELV-AT' => 'Lastschrift Österreich',
    'SUE'    => 'Sofortüberweisung',
    'CB'     => 'ClickAndBuy',
    'INV'    => 'Rechnung',
    'INV-AT' => 'Rechnung Österreich',
    'PAL'    => 'Paypal',
    'PDK'    => 'paydirekt',
    'GP'     => 'giropay',
    'KLA'    => 'Klarna',
    'MPA'    => 'mpass',
    'BAR'    => 'Barzahlen',
    'YAP'    => 'YAPITAL'
  }.freeze

  def price_ex_taxes(base_price, tax)
    return nil unless base_price
    return base_price unless tax || tax.to_i == TAX_FREE
    base_price = base_price.to_d
    tax_rate = tax_rate(tax.to_i)
    base_price / (tax_rate + 1)
  end

  def tax_rate(tax)
    tax = tax.to_i
    TAXES[tax] * 0.01
  end

  def line_item_subtotal(price_ex_tax, quantity)
    price_ex_tax.to_d * quantity.to_i
  end

  def payment_method(code)
    PAYMENT_TYPE[code]
  end
end
