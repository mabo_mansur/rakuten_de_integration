class Order < ActiveRecord::Base
  belongs_to :user

  def sync!
    update_columns(synchronized: true)
  end
end
