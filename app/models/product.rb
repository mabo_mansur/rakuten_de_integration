class Product < ActiveRecord::Base
  belongs_to :user

  def identical?(product_hash)
    data&.deep_symbolize_keys == product_hash&.deep_symbolize_keys
  end
end
