class User < ActiveRecord::Base
  has_many :products, dependent: :destroy
  has_many :orders, dependent: :destroy

  def start_integration
    update(veeqo_channel_active: true)
    InitSyncWorker.perform_in(120, id)
    Producers::Auth.new(self).auth_success
  end

  def activate_veeqo_channel!(reason = '', message = '')
    VeeqoChannel.new(self).activate_by_integration(reason, message)
  end

  def deactivate_veeqo_channel!(reason = '', message = '')
    VeeqoChannel.new(self).deactivate_by_integration(reason, message)
  end
end
