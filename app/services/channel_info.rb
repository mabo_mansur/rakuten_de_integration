module Consumer
  class ChannelInfo
    include Sneakers::Worker
    from_queue 'rakuten.vq.channel.info', env: nil

    def work(msg)
      ChannelInfoWorker.perform_async(msg)
      Sneakers.logger.info("Raw data: ##{msg}")
      ack!
    end
  end
end
