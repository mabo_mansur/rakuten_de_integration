module Producers
  class OrdersUpdate
    include VeeqoHelper
    include LogHelper

    def initialize(user)
      @user = user
      tag_logger("User ##{@user.id}", 'Orders Update')
    end

    def success(received_payload)
      payload = prepare_payload(received_payload).merge!(success: true)
      publish(payload)
      log(:info, "#{received_payload[:action].humanize} success.")
    end

    def failed(received_payload, error_type = '', error_message = '')
      fail = {success: false, error_type: error_type, error_message: error_message}
      payload = prepare_payload(received_payload).merge!(fail)
      publish(payload)
      log(:info, "#{received_payload[:action].humanize} failed #{error_type} #{error_message}")
    end

    private

    def prepare_payload(received_payload)
      {
        resource: 'orders',
        action: received_payload[:action],
        order_id: received_payload[:remote_order_id],
        date: Time.now.utc.strftime('%Y-%m-%d %H:%M %Z')
      }
    end

    def publish(payload)
      message = prepare_message_for_queue(@user).merge!(payload: payload)
      Publisher.publish('rakuten.orders.update.callbacks', message)
    end
  end
end
