module Producers
  class ProductsUpdate
    include VeeqoHelper
    include LogHelper

    def initialize(user)
      @user = user
      tag_logger("User ##{@user.id}", 'Product Update')
    end

    def success(received_payload)
      payload = prepare_payload(received_payload).merge!(success: true)
      message = prepare_message_for_queue(@user).merge!(payload: payload)

      Publisher.publish('rakuten.products.update.callbacks', message)
      log(:info, "#{received_payload[:action].humanize} success.")
    end

    def failed(received_payload, error_type = '', error_message = '')
      fail = {success: false, error_type: error_type, error_message: error_message}
      payload = prepare_payload(received_payload).merge!(fail)
      message = prepare_message_for_queue(@user).merge!(payload: payload)

      Publisher.publish('rakuten.products.update.callbacks', message)
      log(:info, "#{received_payload[:action].humanize} failed #{error_type} #{error_message}")
    end

    private

    def prepare_payload(received_payload)
      {
        resource: 'products',
        action: received_payload[:action],
        product_id: received_payload[:remote_product_id],
        variant_id: received_payload[:remote_variant_id],
        date: Time.current.strftime('%Y-%m-%d %H:%M %Z')
      }
    end
  end
end
