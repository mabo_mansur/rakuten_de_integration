module Producers
  class Auth
    include VeeqoHelper
    include LogHelper

    def initialize(user)
      @user = user
      tag_logger("User ##{@user.id}", 'Settings')
    end

    def auth_success
      message =
        prepare_message_for_queue(@user).
        merge!(payload: {result_code: 'success'})

      PublisherWorker.perform_async('rakuten.authorisation.callbacks', message)
      log(:info, 'Channel authorized')
    end

    def auth_failed(reason = '')
      message =
        prepare_message_for_queue(@user).
        merge!(payload: {result_code: 'failure', result_message: reason})

      PublisherWorker.perform_async('rakuten.authorisation.callbacks', message)
      log(:info, "Channel authorisation failed #{reason}")
    end
  end
end
