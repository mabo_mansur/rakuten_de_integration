module Producers
  class Fetching
    include VeeqoHelper
    include LogHelper

    def initialize(user)
      @user = user
      tag_logger("User ##{@user.id}", 'Callback')
    end

    def failed(resource, error_type = '', error_message = '')
      message = prepare_message_for_queue(@user).merge!(
        payload: {
          resource: resource,
          success: false,
          error_type: error_type,
          error_message: error_message,
          date: Time.now.utc.strftime('%Y-%m-%d %H:%M %Z')
        }
      )

      PublisherWorker.perform_async('rakuten.fetching.callbacks', message)
      log(:info, "#{resource.capitalize} fetching failed #{error_type} #{error_message}")
    end
  end
end
