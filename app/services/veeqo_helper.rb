module VeeqoHelper
  def prepare_message_for_queue(user, resource_name = '', resource = '')
    {
      user_token: user.veeqo_user_token,
      channel_token: user.veeqo_channel_token,
      payload: {
        resource_name.to_sym => resource
      }
    }
  end
end
