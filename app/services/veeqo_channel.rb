class VeeqoChannel
  include VeeqoHelper
  include LogHelper

  def initialize(user)
    @user = user
    tag_logger("User ##{@user.id}", 'Settings')
  end

  def activate_by_integration(reason = '', message = '')
    @user.update(veeqo_channel_active: true)
    message = prepare_message_for_queue(@user).
              merge!(payload:
                {
                  active: true,
                  activation_reason_code: reason,
                  activation_message: message
                })

    PublisherWorker.perform_async('rakuten.activation.callbacks', message)

    log(:info, "Channel activated by integration #{reason}: #{message}")
  end

  def deactivate_by_integration(reason = '', message = '')
    @user.update(veeqo_channel_active: false)
    message = prepare_message_for_queue(@user).
              merge!(payload:
                {
                  active: false,
                  deactivation_reason_code: reason,
                  deactivation_message: message
                })

    PublisherWorker.perform_async('rakuten.activation.callbacks', message)

    log(:info, "Channel deactivated by integration #{reason}: #{message}")
  end

  def apply_veeqo_settings(params)
    @user.update(params)
    @user.reload

    log(:info, "Channel settings updated by veeqo. #{params.map { |k, v| "#{k}: #{v}" }.join(', ')}")
  end

  def activate_by_veeqo
    @user.update(veeqo_channel_active: true)

    log(:info, 'Channel activated by veeqo')
  end

  def deactivate_by_veeqo
    @user.update(veeqo_channel_active: false)

    log(:info, 'Channel deactivated by veeqo')
  end
end
