module Products
  class InitSynchronizer
    include LogHelper
    attr_reader :user

    def initialize(user)
      @user = user
      tag_logger("User ##{@user.id}", 'Products::InitSynchronizer')
    end

    def synchronize
      log(:info, 'Start products pages synchronization')
      page = 1
      response = Products::CollectionSynchronizer.new(user, page).synchronize
      collect_other_pages(response[:total_pages]) if response[:total_pages] > 1
      log(:info, 'Finish products pages synchronization')
    end

    private

    # collect products from second page
    def collect_other_pages(total_pages)
      (2..total_pages).each { |page| Products::CollectionSynchronizerWorker.perform_async(user.id, page) }
    end
  end
end
