module Products
  class CollectionSynchronizer
    include LogHelper

    attr_reader :user, :page

    def initialize(user, page)
      @user = user
      @page = page
      tag_logger("User ##{@user.id}", 'Products::CollectionSynchronizer')
    end

    def synchronize
      response = Rakuten::Client.new(user).products(page)
      status = response[:success]
      if status == Rakuten::Codes::SUCCESS
        products = [] << response[:products][:product]
        sync(products.flatten)
        prepare_response(status, response[:products][:paging][:pages].to_i)
      else
        prepare_response(status)
      end
    rescue StandardError => e
      Producers::Fetching.new(user).failed('products', e.class.name, e.message)
      log(:error, "Error sync products: #{e.message}")
      log(:error, "Backtrace: #{e.backtrace.join("\n")}")
      raise e
    end

    private

    def prepare_response(status, total_pages = 0)
      {success: status, total_pages: total_pages}
    end

    def sync(products)
      products.each do |product_hash|
        product = user.products.find_by(rakuten_id: product_hash[:product_id])
        next if product.present? && product.identical?(product_hash)
        Products::EntitySynchronizerWorker.perform_async(user.id, product_hash)
      end
    end
  end
end
