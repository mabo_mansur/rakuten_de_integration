module Products
  class EntitySynchronizer
    include LogHelper
    include VeeqoHelper

    def initialize(user, product_hash)
      @user = user
      @product_hash = product_hash.deep_symbolize_keys
      tag_logger("User ##{@user.id}", 'Products::EntitySynchronizer')
      log(:info, "product_hash: #{@product_hash}")
    end

    def synchronize
      push_product(create_or_update_product)
      log(:info, 'Finish synchronize product')
    rescue StandardError => e
      log(:error, "Raw rakuten product: #{@product}")
      log(:error, "Error sync product: #{e.message}")
      log(:error, "Backtrace: #{e.backtrace.join("\n")}")
      raise e
    end

    private

    def create_or_update_product
      product = @user.products.find_or_create_by(rakuten_id: @product_hash[:product_id])
      product.update(data: @product_hash)
      product
    end

    def push_product(created_product)
      serialized_product = ProductSerializer.new(@product_hash).prepare
      message = prepare_message_for_queue(@user, 'product', serialized_product)
      log(:info, "Product message body: #{message}")
      Publisher.publish('rakuten.products', message)
      created_product.update(synchronized: true)
      log(:info, 'Product published')
    end
  end
end
