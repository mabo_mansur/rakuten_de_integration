module Products
  class InventorySynchronizer
    include LogHelper

    def initialize(channel_message)
      @payload = channel_message[:payload]
      @user = User.find_by(veeqo_channel_token: channel_message[:channel_token])

      tag_logger("User ##{@user.id}", 'Inventory Sync')
    end

    def synchronize
      log(:info, 'Start sync inventory')
      response = rakuten_request(@payload)
      Producers::ProductsUpdate.new(@user).success(@payload)
      log(:info, 'Finish sync inventory')
      response
    rescue StandardError => e
      Producers::ProductsUpdate.new(@user).failed(@payload, e.class.name, e.message)
      log(:error, "Error update product inventory #{@payload}; #{e.message}")
      log(:error, "Backtrace: #{e.backtrace.join("\n")}")
      raise e
    end

    private

    def rakuten_request(payload)
      if payload[:remote_product_id] == payload[:remote_variant_id]
        Rakuten::Client.new(@user).edit_product(prepare_product_params(payload))
      else
        Rakuten::Client.new(@user).edit_product_variant(prepare_variant_params(payload))
      end
    end

    def prepare_product_params(payload)
      {
        product_id: payload[:remote_product_id],
        stock: payload[:quantity],
        stock_policy: stock_policy(payload[:infinite]),
        available: available(payload[:quantity])
      }
    end

    def prepare_variant_params(payload)
      {
        variant_id: payload[:remote_variant_id],
        stock: payload[:quantity],
        stock_policy: stock_policy(payload[:infinite]),
        available: available(payload[:quantity])
      }
    end

    def stock_policy(infinite)
      infinite ? '0' : '1'
    end

    def available(stock)
      stock.to_i.positive? ? '1' : '0'
    end
  end
end
