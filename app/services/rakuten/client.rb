module Rakuten
  class Client
    include LogHelper

    attr_reader :user, :api

    ITEMS_PER_PAGE = 100

    def initialize(user)
      @user = user
      @api = Rakuten::API.new(user.rakuten_auth_key)
      tag_logger("User ##{@user.id}", 'Rakuten::Client')
    end

    def key_info
      check_access
      log(:info, 'get key info')
      handle_response(api.key_info)
    end

    def products(page)
      check_access
      log(:info, "products | #{page}")
      params = products_params(page)
      handle_response(api.get_products(params))
    end

    def product(id)
      check_access
      log(:info, "one product | #{id}")
      handle_response(api.get_products(search_field: 'product_id', search: id))
    end

    def edit_product(params)
      check_access
      log(:info, "edit product | #{params}")
      handle_response(api.edit_product(params))
    end

    def edit_product_variant(params)
      check_access
      log(:info, "edit variant | #{params}")
      handle_response(api.edit_product_variant(params))
    end

    def orders(status, page)
      check_access
      log(:info, "orders | #{status} | #{page}")
      params = orders_params(status, page)
      handle_response(api.get_orders(params))
    end

    def cancel_order(params)
      check_access
      log(:info, "cancel_order | #{params}")
      handle_response(api.set_order_cancelled(params))
    end

    def ship_order(params)
      check_access
      log(:info, "ship_order | #{params}")
      handle_response(api.set_order_shipped(params))
    end

    private

    def check_access
      return if user.veeqo_channel_active

      log(:info, "deactivated user | #{user.id}")
      raise Rakuten::Errors::DeactivatedUser, "##{user.id} User Deactivated!"
    end

    def products_params(page)
      {per_page: ITEMS_PER_PAGE, page: page}
    end

    def orders_params(status, page)
      {status: status, per_page: ITEMS_PER_PAGE, page: page}
    end

    def handle_response(response_data)
      log(:info, "handle response | #{response_data}")
      if response_data[:success] != Rakuten::Codes::ERROR
        response_data
      else
        error = ([] << response_data[:errors][:error]).flatten.first
        code = error[:code].to_i
        message = error[:message]
        log(:error, "handle error | #{code} | #{message}")
        Rakuten::ErrorHandler.handle(user, code, message)
      end
    end
  end
end
