module Rakuten
  module Codes
    SUCCESS = '1'.freeze
    ERROR = '-1'.freeze
  end

  class API
    include LogHelper

    API_URL = 'http://webservice.rakuten.de/merchants/'.freeze

    attr_reader :key

    def initialize(key = '')
      @key = key
    end

    def key_info
      get('misc/getKeyInfo')
    end

    def get_products(params = {})
      get('products/getProducts', params)
    end

    def edit_product(params)
      post('products/editProduct', params)
    end

    def edit_product_variant(params)
      post('products/editProductMultiVariant', params)
    end

    def get_orders(params = {})
      get('orders/getOrders', params)
    end

    def set_order_cancelled(params)
      post('orders/setOrderCancelled', params)
    end

    def set_order_shipped(params)
      post('orders/setOrderShipped', params)
    end

    private

    def get(endpoint, params = {})
      tag_logger('RakutenClient#GET_request')
      log(:info, "method: GET | url: #{endpoint} | params: #{params}")
      response = HTTParty.get(API_URL + endpoint, query: request_params(params))
      log(:info, "response code #{response.code}")

      response.parsed_response.deep_symbolize_keys[:result]
    end

    def post(endpoint, params = {})
      tag_logger('RakutenClient#POST_request')
      log(:info, "method: POST | url: #{endpoint} | params: #{params}")
      response = HTTParty.post((API_URL + endpoint), query: request_params(params))
      log(:info, "response code #{response.code}")

      response.parsed_response.deep_symbolize_keys[:result]
    end

    def request_params(params)
      {key: key}.merge!(params)
    end
  end
end
