module Rakuten
  module Errors
    ApiError = Class.new(StandardError)
    DeactivatedUser = Class.new(StandardError)
    Unauthorised = Class.new(StandardError)
  end
end
