module Rakuten
  class ErrorHandler
    # 15 The dealer key is invalid
    # 16 The dealer key could not be found
    # 18 The dealer key has been blocked by the Rakuten dealer
    UNATHORISED_CODES = [15, 16, 18].freeze

    def self.handle(user, code, message)
      unauthorise_user!(user, code, message) if UNATHORISED_CODES.include?(code)

      raise Rakuten::Errors::ApiError, "##{code}, #{message}"
    end

    def self.unauthorise_user!(user, code, message)
      user.deactivate_veeqo_channel!('Unauthorised', "##{code}, #{message}")
      raise Rakuten::Errors::Unauthorised, 'Unauthorised Error'
    end

    private_class_method :unauthorise_user!
  end
end
