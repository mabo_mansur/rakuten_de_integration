class Publisher
  def self.publish(queue, message = {})
    start_bunny

    begin
      channel ||= bunny.create_channel.tap(&:confirm_select)
      # grab the queue
      x = channel.queue(queue, durable: true, auto_delete: false)
      # and simply publish message
      result = x.publish(message.to_json, persistent: true)
      published_successfully = channel.wait_for_confirms

      return result if published_successfully

      raise PublishFailure, error_message(message, queue, channel)
    ensure
      channel.close if channel&.open?
    end
  end

  def self.start_bunny
    mutex.synchronize { bunny.start } unless bunny&.connected?
  end

  def self.bunny
    @bunny ||= Bunny.new(Application.config.rabbitmq_uri, continuation_timeout: 30_000)
  end

  def self.mutex
    @mutex ||= Monitor.new
  end

  def error_message(message, queue, channel)
    "Publishing message '#{message.to_json}' into the queue '#{queue}' failed: #{channel.nacked_set.inspect}"
  end

  class PublishFailure < RuntimeError; end
end
