module Orders
  class CancelSynchronizer
    include LogHelper

    def initialize(channel_message)
      @channel_message = channel_message
      @user = User.find_by!(veeqo_channel_token: channel_message[:channel_token])
      tag_logger("User ##{@user.id}", 'Orders Update')
    end

    def to_rakuten
      order_no = @channel_message[:payload][:remote_order_id]
      comment = @channel_message[:payload][:cancel_reason]

      log(:info, "Start cancel ##{order_no}")
      cancel_body = {order_no: order_no, comment: comment}
      Rakuten::Client.new(@user).cancel_order(cancel_body)
      Producers::OrdersUpdate.new(@user).success(@channel_message[:payload])
      log(:info, "Finish cancel #{order_no}")
    rescue StandardError => e
      Producers::OrdersUpdate.new(@user).failed(@channel_message[:payload], e.class.name, e.message)
      log(:error, "Error refund #{@channel_message[:payload]}; #{e.message}")
      log(:error, "Backtrace: #{e.backtrace.join("\n")}")
      raise e
    end
  end
end
