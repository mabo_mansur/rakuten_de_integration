module Orders
  class LineItemsSynchronizer
    include LogHelper

    def initialize(user, order)
      @user = user
      @order = order
      @order_data = order.data.deep_symbolize_keys
      tag_logger("User ##{@user.id}", 'Orders LineItemsSynchronizer')
    end

    def synchronize
      log(:info, "Start line items synchronize ##{@order.rakuten_id}")
      ([] << @order_data[:items][:item]).flatten.each do |line_item|
        product_hash = fetch_from_storage(line_item[:product_id]) || fetch_from_external_api(line_item[:product_id])
        raise Errors::FetchError, 'Product data is nil' unless product_hash.present?
        Products::EntitySynchronizerWorker.perform_async(@user.id, product_hash)
      end
      log(:info, 'Finish line items synchronize')
    rescue StandardError => e
      Producers::Fetching.new(@user).failed('products', e.class.name, e.message)
      log(:error, "Error line items synchronize: #{e.message}")
      log(:error, "Backtrace: #{e.backtrace.join("\n")}")
      raise e
    end

    private

    def fetch_from_storage(product_id)
      log(:info, "Fetch from storage ##{product_id}")
      @user.products.find_by(rakuten_id: product_id)&.data
    end

    def fetch_from_external_api(product_id)
      log(:info, "Fetch from external api ##{product_id}")
      response = Rakuten::Client.new(@user).product(product_id)
      response[:products][:product] if response[:success] == Rakuten::Codes::SUCCESS
    end
  end
end
