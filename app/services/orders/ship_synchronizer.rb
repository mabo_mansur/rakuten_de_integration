module Orders
  class ShipSynchronizer
    include LogHelper

    CARRIERS_LIST = {
      dhl: 'DHL',
      hermes: 'Hermes',
      ups: 'UPS',
      dpd: 'DPD',
      gls: 'GLS',
      deutsche_post_einschreiben: 'Deutsche Post (Einschreiben)',
      post_at: 'Post.at',
      digital: 'Digital',
      spedition: 'Spedition',
      andere: 'Andere'
    }.freeze

    def initialize(channel_message)
      @channel_message = channel_message
      @user = User.find_by!(veeqo_channel_token: channel_message[:channel_token])
      tag_logger("User ##{@user.id}", 'Orders Update')
    end

    def to_rakuten
      log(:info, "Start ship #{ship_body}")
      Rakuten::Client.new(@user).ship_order(ship_body)
      Producers::OrdersUpdate.new(@user).success(@channel_message[:payload])
      log(:info, 'Finish ship')
    rescue StandardError => e
      Producers::OrdersUpdate.new(@user).failed(@channel_message[:payload], e.class.name, e.message)
      log(:error, "Error refund #{@channel_message[:payload]}; #{e.message}")
      log(:error, "Backtrace: #{e.backtrace.join("\n")}")
      raise e
    end

    private

    def ship_body
      order_no = @channel_message[:payload][:remote_order_id]
      shipment = @channel_message[:payload][:shipments]&.first
      carrier = CARRIERS_LIST[shipment[:carrier].parameterize.underscore.to_sym] if shipment.present?

      ship_data = {order_no: order_no}
      ship_data.merge!(carrier: carrier, tracking_number: shipment[:tracking_number]) if carrier.present?
      ship_data
    end
  end
end
