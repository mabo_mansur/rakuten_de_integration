module Orders
  class CollectionSynchronizer
    include LogHelper

    PROCESSING_STATUS = 'editable'.freeze

    attr_reader :user, :page

    def initialize(user, page)
      @user = user
      @page = page
      tag_logger("User ##{@user.id}", 'Orders::CollectionSynchronizer')
    end

    def synchronize
      log(:info, 'Start orders collection synchronization')
      response = Rakuten::Client.new(user).orders(PROCESSING_STATUS, page)
      status = response[:success]
      if status == Rakuten::Codes::SUCCESS
        orders = [] << response[:orders][:order]
        sync(orders.flatten)
        prepare_response(status, response[:orders][:paging][:pages].to_i)
      else
        prepare_response(status)
      end
    rescue StandardError => e
      Producers::Fetching.new(user).failed('orders', e.class.name, e.message)
      log(:error, "Error sync orders: #{e.message}")
      log(:error, "Backtrace: #{e.backtrace.join("\n")}")
      raise e
    end

    private

    def prepare_response(status, total_pages = 0)
      {success: status, total_pages: total_pages}
    end

    def sync(orders)
      orders.each do |order_hash|
        order = user.orders.find_by(rakuten_id: order_hash[:order_no])
        next log(:info, "Skip synchronized order ##{order_hash[:order_no]}") if order&.synchronized

        Orders::EntitySynchronizerWorker.perform_async(user.id, order_hash)
      end
    end
  end
end
