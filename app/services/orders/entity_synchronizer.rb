module Orders
  class EntitySynchronizer
    include LogHelper
    include VeeqoHelper

    def initialize(user, order_hash)
      @user = user
      @order_hash = order_hash.deep_symbolize_keys
      tag_logger("User ##{@user.id}", 'Orders::EntitySynchronizer')
      log(:info, "order_hash = #{@order_hash}")
    end

    def synchronize
      log(:info, 'Start synchronize order')

      order = @user.orders.find_or_create_by(rakuten_id: @order_hash[:order_no]) do |order_entity|
        order_entity.data = @order_hash
      end

      return log(:info, 'Stop synchronize synchronized order') if order.synchronized

      push_order
      order.sync!

      log(:info, 'Finish synchronize order')
    rescue StandardError => e
      log(:error, "Error sync order: #{e.message}")
      log(:error, "Backtrace: #{e.backtrace.join("\n")}")
      raise e
    end

    private

    def push_order
      serialized_order = OrderSerializer.new(@order_hash).serialize
      message = prepare_message_for_queue(@user, 'order', serialized_order)
      log(:info, "Order message body: #{message}")
      Publisher.publish('rakuten.orders.processing', message)
    end
  end
end
