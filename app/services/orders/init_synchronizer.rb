module Orders
  class InitSynchronizer
    include LogHelper
    attr_reader :user

    def initialize(user)
      @user = user
      tag_logger("User ##{@user.id}", 'Orders::InitSynchronizer')
    end

    def synchronize
      log(:info, 'Start orders pages synchronization')
      page = 1
      response = Orders::CollectionSynchronizer.new(user, page).synchronize
      collect_other_pages(response[:total_pages]) if response[:total_pages] > 1
      log(:info, 'Finish orders pages synchronization')
    end

    private

    # collect orders from second page
    def collect_other_pages(total_pages)
      (2..total_pages).each { |page| Orders::CollectionSynchronizerWorker.perform_async(user.id, page) }
    end
  end
end
