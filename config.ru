require './config/boot'
require 'dotenv/load'
require 'sidekiq'
require 'sidekiq/web'

raise TypeError, 'Sidekiq::Web Username or Password missing' unless ENV['SIDEKIQ_PASSWORD'] || ENV['SIDEKIQ_USERNAME']

Sidekiq.configure_client do |config|
  config.redis = {size: 1}
end

map '/sidekiq' do
  use Rack::Auth::Basic, 'Protected Area' do |username, password|
    Rack::Utils.secure_compare(::Digest::SHA256.hexdigest(username), ::Digest::SHA256.hexdigest(ENV['SIDEKIQ_USERNAME'])) &
      Rack::Utils.secure_compare(::Digest::SHA256.hexdigest(password), ::Digest::SHA256.hexdigest(ENV['SIDEKIQ_PASSWORD']))
  end

  run Sidekiq::Web
end

run Rack::URLMap.new('/' => App, '/sidekiq' => Sidekiq::Web)
