class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string  :veeqo_user_token,         null: false
      t.string  :veeqo_channel_token,      null: false
      t.string  :rakuten_auth_key,         null: false
      t.boolean :veeqo_channel_active,     default: false
      t.integer :init_products_count,      default: 0

      t.timestamps null: false
    end
    add_index :users, %i[veeqo_user_token veeqo_channel_token], unique: true
  end
end
