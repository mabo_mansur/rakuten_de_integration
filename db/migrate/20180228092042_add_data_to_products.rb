class AddDataToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :data, :jsonb
  end
end
