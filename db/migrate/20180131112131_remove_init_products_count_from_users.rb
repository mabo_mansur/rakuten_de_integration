class RemoveInitProductsCountFromUsers < ActiveRecord::Migration[5.1]
  def change
    remove_column :users, :init_products_count
  end
end
