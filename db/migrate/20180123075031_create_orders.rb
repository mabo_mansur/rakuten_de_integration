class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.integer :user_id
      t.string  :rakuten_id, limit: 11
      t.boolean :synchronized, default: false

      t.timestamps null: false
    end
    add_index :orders, %i[user_id rakuten_id]
  end
end
