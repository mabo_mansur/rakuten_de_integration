class AddDataToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :data, :jsonb
  end
end
