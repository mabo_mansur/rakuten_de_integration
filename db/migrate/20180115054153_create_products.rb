class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string  :rakuten_id
      t.boolean :synchronized, default: false
      t.integer :user_id
      t.string  :digest

      t.timestamps null: false
    end
    add_index :products, %i[user_id rakuten_id]
  end
end
