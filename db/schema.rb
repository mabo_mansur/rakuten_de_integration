# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180228111028) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "orders", force: :cascade do |t|
    t.integer "user_id"
    t.string "rakuten_id", limit: 11
    t.boolean "synchronized", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "data"
    t.index ["user_id", "rakuten_id"], name: "index_orders_on_user_id_and_rakuten_id"
  end

  create_table "products", force: :cascade do |t|
    t.string "rakuten_id"
    t.boolean "synchronized", default: false
    t.integer "user_id"
    t.string "digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "data"
    t.index ["user_id", "rakuten_id"], name: "index_products_on_user_id_and_rakuten_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "veeqo_user_token", null: false
    t.string "veeqo_channel_token", null: false
    t.string "rakuten_auth_key", null: false
    t.boolean "veeqo_channel_active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["veeqo_user_token", "veeqo_channel_token"], name: "index_users_on_veeqo_user_token_and_veeqo_channel_token", unique: true
  end

end
