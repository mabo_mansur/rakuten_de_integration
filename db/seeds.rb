require 'securerandom'

def rand_key
  SecureRandom.base64(7)
end

unique_numbers = (1..200_000).to_a

20.times do
  User.create(
    veeqo_user_token: rand_key,
    veeqo_channel_token: rand_key,
    rakuten_auth_key: rand_key,
    veeqo_channel_active: [true, false].sample
  )
end

User.all.each do |user|
  5.times do
    Product.find_or_create_by(user_id: user.id, rakuten_id: unique_numbers.shift)
  end
end
